﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Test
{
	internal static class Int32
	{
		public static int ToMillSeconds(this int ms)
		{
			return ms - ms % 10;
		}

		//毫秒化
		public static void MillSecondslize(ref this int ms)
		{
			ms = ms.ToMillSeconds();
		}
	}

	class Program
	{
		static void Main(string[] args)
		{

			int i = 11;
			i.MillSecondslize();
			TimeSpan timeSpan = TimeSpan.FromMilliseconds(1);
			int e = (int)timeSpan.TotalMilliseconds;
			Console.WriteLine(e);

			Console.ReadLine();
		}
	}
}
