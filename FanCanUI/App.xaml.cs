﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FanBox
{
	/// <summary>
	/// App.xaml 的交互逻辑
	/// </summary>
	public partial class App : Application
	{
		public App()
		{
			this.DispatcherUnhandledException += App_DispatcherUnhandledException;
		}

		private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			e.Handled = true;
			MessageBox.Show(e.Exception.Message);
			this.Shutdown(0);
		}

		private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				UIElement ule = sender as UIElement;
				ule.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
			}
		}

		private void TextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			TextBox tb = sender as TextBox;
			tb.SelectionStart = tb.Text.Length;
			tb.SelectAll();
		}
	}
}
