﻿using System;
using System.Windows;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Media.Animation;
using FanBox.FanBoxLib.Models;
using System.Windows.Data;
using System.Globalization;
using System.Collections.Generic;
using ppt = Microsoft.Office.Interop.PowerPoint;
using FanBox.FanBoxLib.Exceptions;
using FanCanCore = FanBox.FanBoxLib.FanCanCore;

namespace FanBox
{
	public class DoubleConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return DependencyProperty.UnsetValue;
			return string.Format("{0:N3}", value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return double.Parse((string)value);
		}
	}

	public class TimeSpanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return DependencyProperty.UnsetValue;
			TimeSpan timeSpanValue = (TimeSpan)value;
			return string.Format("{0:N3}", timeSpanValue.TotalSeconds);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string str = value as string;
			double second;
			if (double.TryParse(str, out second))
			{
				return TimeSpan.FromSeconds(second);
			}

			return DependencyProperty.UnsetValue;
		}
	}

	//public class KeyPointTimeSpanConverter : IValueConverter
	//{
	//	public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
	//	{
	//		KeyPoint keyPoint = value as KeyPoint;
	//		TimeSpan timeSpanValue = keyPoint.Time;
	//		return string.Format("{0:N3}", timeSpanValue.TotalSeconds); ;
	//	}

	//	public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
	//	{
	//		return value;
	//	}
	//}

	public class KeyPointValidationRule : ValidationRule
	{
		private TimeSpan min;
		private TimeSpan max;
		public TimeSpan Min { get => min; set => min = value; }
		public TimeSpan Max { get => max; set => max = value; }
		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			double time = double.Parse((string)value);
			TimeSpan timeSpan = TimeSpan.FromSeconds(time);
			if (timeSpan <= min || timeSpan >= max)
				return new ValidationResult(false, "输入时间非法");
			return new ValidationResult(true, null);
		}
	}

	public partial class FanCanUI : Window
	{
		#region private data members
		private List<string> logs;
		private FanCore fanBoxModel;
		private Slide slide;
		private KeyPoint currentKeyPoint;
		#endregion

		public FanCanUI()
		{
			InitializeComponent();
			logs = new List<string>();
			FanCanCore.LoadSettings(true);
			fanBoxModel = FanCanCore.FanBoxModel;
			slide = FanCanCore.CurrentSlide;

			this.DataContext = slide;

			effectInfoList.ItemsSource = slide.Sequence.Effects;
		}

		private void Register_Click(object sender, RoutedEventArgs e)
		{
			Button btn = sender as Button;
			btn.IsEnabled = false;
			try
			{
				string shpname = FanCanCore.SelectedShape.Name;
				Task.Run(() =>
				{
					FanBoxLib.FanCanCore.SetShapeProperty(shpname, "o", "0");
				});
			}
			catch (Exception ex)
			{
				this.ThrowErrorInTextBlock(ex.Message);
			}
			finally
			{
				Task.Run(() =>
				{
					Dispatcher.Invoke(() => btn.IsEnabled = true);
				});
			}
		}

		private void PreviewCanvasInSlider_Click(object sender, RoutedEventArgs e)
		{
			TimeSpan timeSpan = TimeSpan.FromSeconds(this.previewSlider.Value);
			try
			{
				this.slide.Sequence.Preview(timeSpan);
			}
			catch (Exception ex)
			{
				this.ThrowErrorInTextBlock(ex.Message);
			}

		}
		private void PreviewCanvasLeftMove_Click(object sender, RoutedEventArgs e)
		{
			double m = -0.1;
			previewSlider.Value = Math.Max(previewSlider.Value + m, 0d);
			this.PreviewCanvasInSlider_Click(sender, e);
		}

		private void PreviewCanvasRightMove_Click(object sender, RoutedEventArgs e)
		{
			double m = 0.1;
			previewSlider.Value = Math.Min(previewSlider.Value + m, 30d);
			this.PreviewCanvasInSlider_Click(sender, e);
		}

		private void PreviewCanvasInKeyPoint_Click(object sender, RoutedEventArgs e)
		{
			Control menuItem = sender as Control;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			TimeSpan timeSpan = keyPoint.Time;
			try
			{
				this.slide.Sequence.Preview(timeSpan);
			}
			catch (Exception ex)
			{
				this.ThrowErrorInTextBlock(ex.Message);
			}
		}

		private void AddEmpty_Click(object sender, RoutedEventArgs e)
		{
			Button btn = sender as Button;
			btn.Visibility = Visibility.Visible;
			btn.IsEnabled = false;

			Task.Run(() =>
			{
				FanCanCore.SetShapeProperty("空图层", "o", "0");
				Dispatcher.Invoke(() => btn.IsEnabled = true);
			});
		}

		private void SlideShow_Click(object sender, RoutedEventArgs e)
		{
			Button btn = sender as Button;
			btn.IsEnabled = false;
			Task.Run(() =>
			{
				FanCanCore.OpenSlideShow();
				Dispatcher.Invoke(() => btn.IsEnabled = true);
			});
		}

		private void PlayShow_Click(object sender, RoutedEventArgs e)
		{
			Button btn = sender as Button;
			btn.IsEnabled = false;
			Task.Run(() =>
			{
				FanCanCore.PlaySlideShow();
				Dispatcher.Invoke(() => btn.IsEnabled = true);
			});

		}

		private void HideAllBehavior_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Effect eff = (Effect)menuItem.DataContext;
			eff.IsVisible = Visibility.Collapsed;
		}

		private void ShowAllBehavior_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Effect eff = (Effect)menuItem.DataContext;
			eff.IsVisible = Visibility.Visible;
		}

		private void AddCurrentKeyPoint_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Behavior bhv = menuItem.DataContext as Behavior;
			Effect eff = bhv.Effect;
			TimeSpan timeSpan = TimeSpan.FromSeconds(this.previewSlider.Value);
			Shape shp = bhv.Effect.Shape;
			double value = 0d;
			switch (bhv.Property)
			{
				case ppt.MsoAnimProperty.msoAnimX:
					value = shp.X - eff.X;
					break;
				case ppt.MsoAnimProperty.msoAnimY:
					value = shp.Y - eff.Y;
					break;
				case ppt.MsoAnimProperty.msoAnimWidth:
					value = shp.Width - eff.Width;
					break;
				case ppt.MsoAnimProperty.msoAnimHeight:
					value = shp.Height - eff.Height;
					break;
				default:
					value = 0;
					break;
			}
			value = Math.Round(value, 3);
			try
			{
				bhv.AddPoint(timeSpan, value.ToString(), "$");
			}
			catch (Exception ex)
			{
				this.ThrowErrorInTextBlock(ex.Message);
			}


		}

		private void ResetProperty_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Behavior bhv = menuItem.DataContext as Behavior;
			Effect eff = bhv.Effect;
			Shape shp = bhv.Effect.Shape;
			double value = 0d;
			switch (bhv.Property)
			{
				case ppt.MsoAnimProperty.msoAnimX:
					value = shp.X;
					break;
				case ppt.MsoAnimProperty.msoAnimY:
					value = shp.Y;
					break;
				case ppt.MsoAnimProperty.msoAnimWidth:
					value = shp.Width;
					break;
				case ppt.MsoAnimProperty.msoAnimHeight:
					value = shp.Height;
					break;
				default:
					value = 0;
					break;
			}
			value = Math.Round(value, 3);
			try
			{
				bhv.Value = value;
			}
			catch (Exception ex)
			{
				this.ThrowErrorInTextBlock(ex.Message);
			}
		}

		//private void ResetProperty_Click(object sender, RoutedEventArgs e)
		//{
		//	MenuItem menuItem = sender as MenuItem;
		//	Behavior bhv = menuItem.DataContext as Behavior;
		//	Effect eff = bhv.Effect;
		//	Shape shp = bhv.Effect.Shape;
		//	double value = 0d;
		//	switch (bhv.Property)
		//	{
		//		case ppt.MsoAnimProperty.msoAnimX:
		//			value = shp.X;
		//			break;
		//		case ppt.MsoAnimProperty.msoAnimY:
		//			value = shp.Y;
		//			break;
		//		case ppt.MsoAnimProperty.msoAnimWidth:
		//			value = shp.Width;
		//			break;
		//		case ppt.MsoAnimProperty.msoAnimHeight:
		//			value = shp.Height;
		//			break;
		//		default:
		//			value = 0;
		//			break;
		//	}
		//	value = Math.Round(value, 3);
		//	try
		//	{
		//		bhv.Value = value;
		//	}
		//	catch (Exception ex)
		//	{
		//		this.ThrowErrorInTextBlock(ex.Message);
		//	}


		//}
		private void PasteKeyPoint_Click(object sender, RoutedEventArgs e)
		{
			if (currentKeyPoint == null) return;
			MenuItem menuItem = sender as MenuItem;
			Behavior bhv = menuItem.DataContext as Behavior;
			KeyPoint collepsedKeyPoint = null;
			foreach (KeyPoint point in bhv.Points)
			{
				if (point.Time > currentKeyPoint.Time)
					break;
				if (point.Time == currentKeyPoint.Time)
				{
					collepsedKeyPoint = point;
					break;
				}
			}
			try
			{
				if (collepsedKeyPoint == null)
				{
					TimeSpan newTimeSpan = currentKeyPoint.Time;
					currentKeyPoint.Copy(newTimeSpan, bhv);
				}
				else
				{
					KeyPoint point1 = currentKeyPoint;
					KeyPoint point2 = currentKeyPoint.NextPoint;
					if (point2 == null)
					{
						point1 = currentKeyPoint.PrevPoint;
						point2 = currentKeyPoint;
					}
					TimeSpan newTimeSpan = TimeSpan.FromSeconds
						((point2.Time.TotalSeconds + point1.Time.TotalSeconds) / 2);
					currentKeyPoint.Copy(newTimeSpan, bhv);
				}
			}
			catch(Exception ex)
			{
				this.ThrowErrorInTextBlock(ex.Message);
			}
		}

		private void HideBehaviorPoint_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Behavior bhv = menuItem.DataContext as Behavior;
			bhv.IsVisible = Visibility.Collapsed;
		}

		private void CopyKeyPoint_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			currentKeyPoint = keyPoint;
		}


		#region 预设事件
		private void PresetLinearBegin_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			keyPoint.Formula = "$";
		}
		private void PresetRandomBegin_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Value = $"{infactor}*(0.5-rand(1))";
			keyPoint.Formula = "$";
		}

		private void PresetRandomTwinkle_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			//string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Formula = $"100*(0.5-rand(1))";
		}

		private void PresetSinWave_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			//string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Value = "0";
			if (keyPoint.NextPoint != null)
				keyPoint.NextPoint.Value = "1";
			keyPoint.Formula = $"100*sin((2*pi)*$)";
		}

		private void PresetCosWave_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			//string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Value = "0";
			if (keyPoint.NextPoint != null)
				keyPoint.NextPoint.Value = "1";
			keyPoint.Formula = $"100*cos((2*pi)*$)";
		}

		private void PresetToothedWave_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			//string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Value = "0";
			if (keyPoint.NextPoint != null)
				keyPoint.NextPoint.Value = "1";
			keyPoint.Formula = $"100*($-floor($))";
		}

		private void PresetRectangleWave_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			//string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Value = "0";
			if (keyPoint.NextPoint != null)
				keyPoint.NextPoint.Value = "1";
			keyPoint.Formula = $"100*((-1)^floor(2*$))";
		}

		private void PresetTriangleWave_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			//string infactor = string.Format("{0:N3}", Screen.GetProperty(keyPoint));
			keyPoint.Value = "0";
			if (keyPoint.NextPoint != null)
				keyPoint.NextPoint.Value = "1";
			string ffuncfmt = "((0-1)^floor(2*({0}))*1+1)";//0,0.5=> 1, 0
			string gpfuncfmt = "(({0})-floor(({0})))";//0~0.5 =>0~1
			string gmfuncfmt = "(0.5-(({0})-floor(({0}))))";//0~0.5 =>0~0.5
			string itemf1 = string.Format(ffuncfmt, "$+0.25");
			string itemf2 = string.Format(ffuncfmt, "$-0.25");
			string itemg1 = string.Format(gpfuncfmt, "$+0.25");
			string itemg2 = string.Format(gmfuncfmt, "$-0.25");
			string rst = $"100*(2*({itemf1}*{itemg1}+{itemf2}*{itemg2})-1)";
			keyPoint.Formula = rst;
		}

		#endregion

		private void CloseShow_Click(object sender, RoutedEventArgs e)
		{
			Button btn = sender as Button;
			btn.IsEnabled = false;
			Task.Run(() =>
			{
				FanCanCore.CloseSlideShow();
				Dispatcher.Invoke(() => btn.IsEnabled = true);
			});
		}

		private void TopMost_Click(object sender, RoutedEventArgs e)
		{
			this.Topmost = !this.Topmost;
		}

		private void DeleteKeyPoint_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;

			try
			{
				keyPoint.Delete();

			}
			catch (Exception ex)
			{
				ThrowErrorInTextBlock(ex.Message);
			}

		}

		private void DeleteEffect_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Effect effect = menuItem.DataContext as Effect;

			effect.Delete();

		}

		private void AddKeyPoint_Click(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			KeyPoint keyPoint = menuItem.DataContext as KeyPoint;
			Behavior bhv = keyPoint.Points.Behavior;

			KeyPoint point1 = keyPoint;
			KeyPoint point2 = keyPoint.NextPoint;
			if (point2 == null)
			{
				point1 = keyPoint.PrevPoint;
				point2 = keyPoint;
			}
			double currentTimeValue = point1.Time.TotalSeconds;
			double newTimeDelta = (point2.Time.TotalSeconds - point1.Time.TotalSeconds);

			if (newTimeDelta <= 0.25d)
			{
				currentTimeValue = (point2.Time.TotalSeconds + point1.Time.TotalSeconds) / 2d;
			}
			else
			{
				currentTimeValue = currentTimeValue + 0.25d;
			}

			bhv.AddPoint(TimeSpan.FromSeconds(currentTimeValue), "0", "$");

		}

		private void TimeTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			TextBox textBox = sender as TextBox;
			ContentPresenter parentStackPanel = textBox.TemplatedParent as ContentPresenter;

			KeyPoint keyPoint = parentStackPanel.DataContext as KeyPoint;

			string text = textBox.Text;
			double time;

			if (double.TryParse(text, out time))
			{
				TimeSpan timeSpan = TimeSpan.FromSeconds(time);
				try
				{
					keyPoint.Time = timeSpan;
				}
				catch (Exception ex)
				{
					this.ThrowErrorInTextBlock(ex.Message);
					return;
				}
			}
			else
			{
				this.ThrowErrorInTextBlock("非法值");
			}
		}

		private void ExportLog_Click(object sender, RoutedEventArgs e)
		{
			BugLog.Export();
		}

		private void CheckBox_Checked(object sender, RoutedEventArgs e)
		{
			CheckBox checkBox = sender as CheckBox;

			if (checkBox.IsChecked == true)
				FanCanCore.OpenSlideShow();
			else
				FanCanCore.CloseSlideShow();
		}

		private void PreviewSlider_DragCompleted(object sender, DragCompletedEventArgs e)
		{
			this.PreviewCanvasInSlider_Click(sender, e);
		}

		private void ThrowErrorInTextBlock(string errorText)
		{
			stateTextBlock.Text = errorText;
			ColorAnimation warningPopAnim = new ColorAnimation(Colors.Red, Colors.Black, new Duration(TimeSpan.FromSeconds(0.5)));
			SolidColorBrush newColorBrush = new SolidColorBrush(Colors.Red);
			stateTextBlock.Foreground = newColorBrush;
			newColorBrush.BeginAnimation(SolidColorBrush.ColorProperty, warningPopAnim);

		}

	}
}
