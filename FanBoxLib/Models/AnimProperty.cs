﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace FanBox.FanBoxLib.Models
{
	//单一属性入口
	public class AnimProperty:INotifyPropertyChanged
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal EnumProperty propertyType;
		internal Shape parentShape;
		internal Behavior propBehavior;
		internal Behavior animBehavior;
		internal List<Behavior> childBehaviorsList;
		#endregion

		#region ctors
		internal AnimProperty
			(Shape hostShape, EnumProperty property, Behavior propBehavior, Behavior animBehavior)
		{
			this.propertyType = property;
			this.parentShape = hostShape;
			this.propBehavior = propBehavior;
			this.animBehavior = animBehavior;
			this.childBehaviorsList = new List<Behavior>();
		}
		#endregion

		#region public methods
		
		#endregion

		#region private methods
		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region static methods

		//添加动画属性，用于新建一个PPT完整的动画
		internal static void AddAnimProperty
			(Shape hostShape,EnumProperty property, Behavior propBehavior, Behavior animBehavior)
		{
			AnimProperty animprop = new AnimProperty(hostShape, property, propBehavior, animBehavior);
			hostShape.propertyDic.Add(property, animprop);
		}


		internal static void Preset(AnimProperty animProperty)
		{
			Behavior.Preset(animProperty.propBehavior);
		}

		internal static void Preview(AnimProperty animProperty, int millseconds)
		{
			Behavior.Preview(animProperty.animBehavior, millseconds);
		}

		//添加一条动画的引用。
		internal static void AddRefBehavior(AnimProperty sourceAnimProp, AnimProperty targetAnimProp)
		{
			//TODO 让源动画的动画行为与子行为向此处建立映射
			//向指定图形的ChildEffect中注册建立引用的Behavior（CreateBone）
			//将建立引用的Behavior装载到targetAnimProp中。

			Effect targetEffect = targetAnimProp.parentShape.childEffect;

			Behavior newbehavior = 
				Behavior.CreateBindedBehavior(sourceAnimProp.animBehavior, targetEffect, targetAnimProp.propertyType);
			targetAnimProp.childBehaviorsList.Add(newbehavior);

			foreach (Behavior behavior in sourceAnimProp.childBehaviorsList)
			{
				newbehavior = Behavior.CreateBindedBehavior(behavior, targetEffect, targetAnimProp.propertyType);
				targetAnimProp.childBehaviorsList.Add(newbehavior);
			}

		}
		#endregion
	}
}
