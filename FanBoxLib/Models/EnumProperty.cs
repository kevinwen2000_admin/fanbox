﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ppt = Microsoft.Office.Interop.PowerPoint;
namespace FanBox.FanBoxLib.Models
{
	public enum EnumProperty
	{
		X,
		Y,
		Width,
		Height,
		Rotation,
		Opacity,
	}

	internal static class PropertyConverter
	{
		internal static ppt.MsoAnimProperty Convert(EnumProperty enumProperty)
		{
			switch (enumProperty)
			{
				case EnumProperty.X:
					return ppt.MsoAnimProperty.msoAnimX;
				case EnumProperty.Y:
					return ppt.MsoAnimProperty.msoAnimY;
				case EnumProperty.Width:
					return ppt.MsoAnimProperty.msoAnimWidth;
				case EnumProperty.Height:
					return ppt.MsoAnimProperty.msoAnimHeight;
				case EnumProperty.Rotation:
					return ppt.MsoAnimProperty.msoAnimRotation;
				case EnumProperty.Opacity:
					return ppt.MsoAnimProperty.msoAnimOpacity;
				default:
					return ppt.MsoAnimProperty.msoAnimNone;
			}
		}
	}

	internal static class PropertyHelper
	{
		public readonly static List<string> PropertyNames =
			new List<string>() { "width", "height", "x", "y", "rotation", "opacity" };
		public readonly static List<EnumProperty> PropertyTypesArr = new List<EnumProperty>()
		{
			EnumProperty.Width,
			EnumProperty.Height,
			EnumProperty.X,
			EnumProperty.Y
		};
	}

}
