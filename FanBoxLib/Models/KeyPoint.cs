﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FanBox.FanBoxLib.Tools;
using System.Collections.ObjectModel;
using System.Windows.Threading;

using ppt = Microsoft.Office.Interop.PowerPoint;
using FanBox.FanBoxLib.Exceptions;

namespace FanBox.FanBoxLib.Models
{
	internal static class KeyPointBugLog
	{
		public static void MoveTime(KeyPoint keyPoint, int millseconds)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointMoveTime,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，修改帧时{3}。
			BugLog.Add(output);
		}

		public static void AdjustTime(KeyPoint keyPoint, int millseconds)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointAdjustTime,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，修改帧时{3}。
			BugLog.Add(output);
		}

		public static void VisitTime(KeyPoint keyPoint, int millseconds)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointVisitTime,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，修改帧时{3}。
			BugLog.Add(output);
		}

		public static void VisitValue(KeyPoint keyPoint, string value)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointVisitValue,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				value);
			//图形{0}属性{1}位于{2}秒关键帧，修改帧时{3}。
			BugLog.Add(output);
		}

		public static void VisitFormula(KeyPoint keyPoint, string formula)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointVisitFormula,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				formula);
			//图形{0}属性{1}位于{2}秒关键帧，修改帧时{3}。
			BugLog.Add(output);
		}

		public static void AddKeyPoint(KeyPoints keyPoints, int millseconds, string value, string formula)
		{
			//KeyPoints keyPoints = keyPoint.Points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointAddKeyPoint,
				effect.shape.Name,
				behavior.Name,
				millseconds,
				value,
				formula);
			//图形{0}属性{1}，添加帧时{2}，帧值{3}，帧表达式{4}
			BugLog.Add(output);
		}

		public static void DeleteKeyPoint(KeyPoint keyPoint)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointDelete,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，删除。
			BugLog.Add(output);
		}

		public static void CopyKeyPoint(KeyPoint keyPoint, int millseconds)
		{

			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointCopy,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，拷贝帧时{3}。
			BugLog.Add(output);
		}

		public static void CopyToKeyPoint(KeyPoint keyPoint, KeyPoints points, int millseconds)
		{
			KeyPoints keyPoints = keyPoint.points;
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;

			Behavior targetBehavior = points.behavior;
			Effect targetEffect = targetBehavior.effect;

			string output = string.Format
				(Resouces.LogMessages.KeyPointCopyTo,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds,
				targetEffect.shape.Name,
				targetBehavior.Name,
				millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，拷贝到图形{3}属性{4}帧时{5}
			BugLog.Add(output);
		}
	}

	public class KeyPoint : INotifyPropertyChanged
	{
		#region const
		private const int TWEENS_MIN_COUNT = 10;//最少补间数
		#endregion

		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal KeyPoints points;//父结点
		private ppt.AnimationPoint pptPoint;

		internal int millseconds;//具体帧时，精确到毫秒
		internal string value;
		internal string formula;

		private bool isLocked;//是否被锁定，被锁定的帧点无法移动也无法被删除，具体指收尾两端

		//补间相关
		//补间点不在点集之中
		private SmoothPoint smoothIn;//平滑开始
		private SmoothPoint smoothOut;//平滑结束

		internal KeyPoint previousPoint;//前向点
		internal KeyPoint nextPoint;//后向点

		private Script script; //脚本表达式对象
		private bool isTween;//是否由脚本补间生成，由补间生成的帧点不可见，只有首尾补间帧点可见
		private KeyPoint tweenParent;//补间点的父点
		private List<KeyPoint> tweenList;//补间点集合

		//骨骼绑定相关
		//IsIndividual 是否为绑骨独立点
		private KeyPoint parentPoint;//用于骨骼绑定的父帧点
		private List<KeyPoint> childPointList;//用于骨骼绑定的子帧点集合

		//实时预览相关
		internal Ele[] valueElems;
		internal Ele[] formulaElems;

		#endregion

		#region public properties
		internal Dispatcher UIDispatcher
		{
			get { return this.points.UIDispatcher; }
		}

		public KeyPoint PrevPoint
		{
			get { return this.previousPoint; }
		}

		public KeyPoint NextPoint
		{
			get { return this.nextPoint; }
		}

		//平滑开始
		public SmoothPoint SmoothIn
		{
			get { return this.smoothIn; }

		}

		//平滑结束
		public SmoothPoint SmoothOut
		{
			get { return this.smoothOut; }
		}

		public int Millseconds
		{
			get => KeyPoint.GetTime(this);
			set
			{
				KeyPoint.SetTime(this, value);
				this.PropertyChangedInvoke("Millseconds");
			}
		}

		public string Value
		{
			get => KeyPoint.GetValue(this);
			set
			{
				KeyPoint.SetValue(this, value);
				this.PropertyChangedInvoke("Value");
			}
		}

		public string Formula
		{
			get => KeyPoint.GetFormula(this);
			set
			{
				KeyPoint.SetFormula(this, value);
				this.PropertyChangedInvoke("Formula");
			}
		}

		public Script Script
		{
			get { return this.script; }
		}

		public bool IsLocked
		{
			get { return this.isLocked; }
		}

		//是否为补间帧点
		public bool IsTween
		{
			get { return this.isTween; }
		}

		//是否是空图层的帧点，用于骨骼绑定
		public bool IsEmptyPoint
		{
			get { return this.points.IsEmptyPoints; }
		}

		//是否为骨骼独立点，如果是独立点，那它将可被编辑
		public bool IsIndividual
		{
			get { return this.points.IsIndividual; }
		}

		#endregion

		#region ctors
		internal KeyPoint(
			Behavior behavior,
			ppt.AnimationPoint point,
			KeyPoint prevp, KeyPoint nextp,
			int millseconds,
			bool isLocked = false) :
			this(behavior.points, point, prevp, nextp, millseconds, isLocked)
		{ }


		internal KeyPoint
			(KeyPoints points, ppt.AnimationPoint point, KeyPoint prevp, KeyPoint nextp,
			int millseconds, bool isLocked = false)
		{
			this.parentPoint = null;
			this.childPointList = new List<KeyPoint>();
			this.tweenList = new List<KeyPoint>();
			this.script = new Script();

			this.smoothIn = new SmoothPoint();
			this.smoothOut = new SmoothPoint();

			this.points = points;
			this.pptPoint = point;

			this.previousPoint = prevp;
			this.nextPoint = nextp;

			this.millseconds = millseconds;
			this.value = "0";
			this.formula = "$";
			this.valueElems = new Ele[] { new Ele(value, EnumEleType.Number) };
			this.formulaElems = new Ele[] { new Ele("$", EnumEleType.Number) };
			this.isLocked = isLocked;
		}

		#endregion

		#region public methods
		public void Delete()
		{
			KeyPointBugLog.DeleteKeyPoint(this);

			#region 异常处理
			if (this.isLocked == true)
			{
				BugLog.ThrowError(new KeyPointLockProtectedException());
			}

			if (this.IsIndividual == false)
			{
				BugLog.ThrowError(new KeyPointIndividualProtectedException());
			}

			if (this.isTween == true)
			{
				BugLog.ThrowError(new KeyPointTweenProtectedException());
			}
			#endregion

			KeyPoint.DeleteKeyPoint(this);
		}

		public void Copy(int millseconds)
		{
			KeyPoints keyPoints = this.points;
			KeyPoint.CopyKeyPoint(this, keyPoints, millseconds);
		}

		public void Copy(int millseconds, Behavior behavior)
		{
			KeyPoints keyPoints = behavior.points;
			KeyPoint.CopyKeyPoint(this, keyPoints, millseconds);
		}

		public void CreateTweens()
		{
			KeyPoint.CreateTweens(this);
		}

		public override string ToString()
		{
			string nl = $"{Environment.NewLine}";
			string head = $"[Point]";
			string timeStr = $"time:{this.millseconds}ms";
			string valueStr = $"value:{this.value,-8}";
			string formulaStr = $"formula:{this.formula}";
			//string codeInfoStr = $"lang:{this.script.Language}\tscript:{this.script.Language}";
			//string smooth = $"inx {smoothIn.X} iny {smoothIn.Y} outx {SmoothOut.X} outy {smoothOut.X}";

			return $"{head}\t{timeStr}\t{valueStr}\t{formulaStr}";
		}
		#endregion

		#region private methods

		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion

		#region static methods

		#region default
		//创建调整过的值
		private static string CreateAdjustedValue(KeyPoint keyPoint, string value)
		{
			double adjvalue = Screen.GetProperty(keyPoint.points.behavior.property);
			return $"(  {value}  )/{adjvalue}";
		}
		private static string CreateAdjustedValue(KeyPoints keyPoints, string value)
		{
			double adjvalue = Screen.GetProperty(keyPoints.behavior.property);
			return $"(  {value}  )/{adjvalue}";
		}
		internal static float GetPptTimePrecent(KeyPoint keyPoint, int millseconds)
		{
			return KeyPoints.GetPptTimePrecent(keyPoint.points, millseconds);
		}
		#endregion

		#region properties 

		#region	time

		private static int GetTime(KeyPoint keyPoint)
		{
			if (keyPoint.IsIndividual)
				return keyPoint.millseconds;
			else
				return KeyPoint.GetTime(keyPoint.parentPoint);
		}

		//为独立点设置属性
		private static void SetTime(KeyPoint keyPoint, int millseconds)
		{
			if (keyPoint.IsIndividual == false)
				return;
			millseconds.MillSecondslize();

			KeyPoints points = keyPoint.points;

			#region 异常处理
			if (keyPoint.millseconds == millseconds)
				return;

			if (keyPoint.isLocked == true)
				BugLog.ThrowError(new KeyPointLockProtectedException());

			if (keyPoint.IsIndividual == false)
				BugLog.ThrowError(new KeyPointIndividualProtectedException());

			if (KeyPoints.IsInDurationRange(points, millseconds) == false)
				BugLog.ThrowError(new KeyPointsNewTimeOutOfRangeException());

			#endregion

			int prevTime = keyPoint.points.behavior.headPoint.millseconds;
			int nextTime = keyPoint.points.behavior.rearPoint.millseconds;

			if (keyPoint.previousPoint != null)
				prevTime = keyPoint.previousPoint.millseconds;

			if (keyPoint.nextPoint != null)
				nextTime = keyPoint.nextPoint.millseconds;

			if (keyPoint.previousPoint == null && keyPoint.nextPoint == null)
				keyPoint.millseconds = millseconds;

			else if (millseconds < prevTime || millseconds > nextTime)
				KeyPoint.MoveKeyPoint(keyPoint, millseconds);
			else
				KeyPoint.AdjustKeyPoint(keyPoint, millseconds);
		}

		#region adjust time
		//按照比例调整帧点
		//TODO 调整区间如果比原先的小，那么就需要考虑到帧点描述的精确性。
		//补间帧点是不考虑10ms的精度的，以此来保证补间帧点能够正确地被调整
		//同时，我们希望能够比较动态地生成补间帧点
		//我们希望为1s生成25个帧点，2s生成50个帧点
		//速度曲线只能用在帧点系统上，如果采用了表达式，那速度曲线就被禁用掉
		private static void AdjustKeyPoint(KeyPoint keyPoint, int millseconds)
		{
			//TODO KeyPointBugLog.AdjustTime(keyPoint, millseconds);
			millseconds.MillSecondslize();
			KeyPoint prevKeyPoint = keyPoint.previousPoint;
			KeyPoint nextKeyPoint = keyPoint.nextPoint;

			int prevt = prevKeyPoint.millseconds;
			int nextt = nextKeyPoint.millseconds;
			int oldt = keyPoint.millseconds;
			int newt = millseconds;
			int oldPrevDelta = oldt - prevt;
			int oldNextDelta = nextt - oldt;
			int newPrevDelta = newt - prevt;
			int newNextDelta = nextt - newt;

			float pptprevt = prevKeyPoint.pptPoint.Time;
			float pptnextt = nextKeyPoint.pptPoint.Time;
			float pptoldt = keyPoint.pptPoint.Time;
			float pptnewt = KeyPoint.GetPptTimePrecent(keyPoint, millseconds);
			float pptoldPrevDelta = pptoldt - pptprevt;
			float pptoldNextDelta = pptnextt - pptoldt;
			float pptnewPrevDelta = pptnewt - pptprevt;
			float pptnewNextDelta = pptnextt - pptnewt;

			keyPoint.millseconds = newt;
			keyPoint.pptPoint.Time = pptnewt;


			//调整子序列
			foreach (KeyPoint child in keyPoint.childPointList)
			{
				KeyPoint.AdjustKeyPointBinded(child, pptnewt);
			}

			//修改前趋补间点
			foreach (KeyPoint tween in keyPoint.previousPoint.tweenList)
			{
				float pptoldtt = tween.pptPoint.Time;
				float pptoldTweenDelta = pptoldtt - pptprevt;
				float p = pptoldTweenDelta / oldPrevDelta;
				float pptnewtt = (pptprevt + p * pptnewPrevDelta);

				KeyPoint.AdjustKeyPointBinded(tween, pptnewtt);

				//tween.Time_ = TimeSpan.FromSeconds(newtt);
			}

			//修改后趋补间点
			foreach (KeyPoint tween in keyPoint.tweenList)
			{
				float pptoldtt = tween.pptPoint.Time;
				float pptoldTweenDelta = pptoldtt - pptoldt;
				float p = pptoldTweenDelta / pptoldNextDelta;
				float pptnewtt = pptoldt + p * pptnewNextDelta;

				KeyPoint.AdjustKeyPointBinded(tween, pptnewtt);
			}

		}

		private static void AdjustKeyPointBinded(KeyPoint keyPoint, float pptTime)
		{
			keyPoint.pptPoint.Time = pptTime;
			foreach (KeyPoint childPoint in keyPoint.childPointList)
			{
				KeyPoint.AdjustKeyPointBinded(childPoint, pptTime);
			}
		}

		//移动帧点到别的地方，帧点移动前需要删除所有的补间
		private static void MoveKeyPoint(KeyPoint keyPoint, int millseconds)
		{
			//TODO KeyPointBugLog.MoveTime(keyPoint, millseconds);

			var keyPoints = keyPoint.points;
			var value = keyPoint.value;
			var formula = keyPoint.formula;
			var pptformula = KeyPoint.CreateAdjustedValue(keyPoint, formula);
			var isLocked = keyPoint.isLocked;
			var tweenParent = keyPoint.tweenParent;
			var script = keyPoint.script;
			var smoothIn = keyPoint.smoothIn;
			var smoothOut = keyPoint.smoothOut;

			KeyPoint.DeleteKeyPoint(keyPoint);

			KeyPoint newKeyPoint = KeyPoint.AddNormalPoint(keyPoints, millseconds);
			newKeyPoint.value = value;
			newKeyPoint.formula = formula;
			if (keyPoint.IsEmptyPoint == false)
			{
				newKeyPoint.pptPoint.Value = value;
				newKeyPoint.pptPoint.Formula = pptformula;
			}
			newKeyPoint.smoothIn = new SmoothPoint(smoothIn);
			newKeyPoint.smoothOut = new SmoothPoint(smoothOut);
			newKeyPoint.script = new Script(script);

			foreach (KeyPoint childPoint in newKeyPoint.childPointList)
			{
				KeyPoint.MoveKeyPointBinded(keyPoint, value, pptformula);
			}
			//CreateTweens(newKeyPoint);
		}

		private static void MoveKeyPointBinded(KeyPoint keyPoint, string value, string pptformula)
		{
			if (keyPoint.IsEmptyPoint == false)
			{
				keyPoint.pptPoint.Value = value;
				keyPoint.pptPoint.Value = pptformula;
			}

			foreach (KeyPoint childPoint in keyPoint.childPointList)
			{
				KeyPoint.MoveKeyPointBinded(keyPoint, value, pptformula);
			}
			//CreateTweens(newKeyPoint);
		}

		#endregion

		#endregion

		#region value
		private static string GetValue(KeyPoint keyPoint)
		{
			if (keyPoint.IsIndividual)
				return keyPoint.value;
			else
				return KeyPoint.GetValue(keyPoint.parentPoint);
		}

		private static void SetValue(KeyPoint keyPoint, string valueExpression)
		{
			if (keyPoint.IsIndividual == false) return;
			var valueExp = valueExpression.Trim();
			if (valueExp == keyPoint.value)
				return;
			var elems = FormulaAnalyzer.Splite(valueExpression);
			var parsedValue = FormulaAnalyzer.Parse(elems);
			keyPoint.value = valueExp;
			keyPoint.valueElems = elems;
			keyPoint.pptPoint.Value = parsedValue;

			foreach (KeyPoint child in keyPoint.childPointList)
			{
				KeyPoint.SetValueBinded(child, parsedValue);
			}

		}

		private static void SetValueBinded(KeyPoint keyPoint, string parsedValue)
		{
			keyPoint.pptPoint.Value = parsedValue;
			foreach (KeyPoint child in keyPoint.childPointList)
			{
				KeyPoint.SetValueBinded(child, parsedValue);
			}
		}

		#endregion

		#region formula
		private static string GetFormula(KeyPoint keyPoint)
		{
			if (keyPoint.IsIndividual)
				return keyPoint.formula;
			else
				return KeyPoint.GetFormula(keyPoint.parentPoint);
		}

		private static void SetFormula(KeyPoint keyPoint, string formulaExpression)
		{
			if (keyPoint.IsIndividual == false) return;

			var formulaExp = formulaExpression.Trim();

			if (formulaExp == keyPoint.formula) return;
			var elems = FormulaAnalyzer.Splite(formulaExpression);
			var parsedFormula = FormulaAnalyzer.Parse(elems);
			keyPoint.formula = formulaExp;
			keyPoint.formulaElems = elems;
			keyPoint.pptPoint.Formula = parsedFormula;

			foreach (KeyPoint child in keyPoint.childPointList)
			{
				KeyPoint.SetFormulaBinded(child, parsedFormula);
			}

		}

		private static void SetFormulaBinded(KeyPoint keyPoint, string parsedFormula)
		{
			keyPoint.pptPoint.Value = parsedFormula;
			foreach (KeyPoint child in keyPoint.childPointList)
			{
				KeyPoint.SetFormulaBinded(child, parsedFormula);
			}
		}

		#endregion

		#endregion

		#region tweens

		//创建补间（覆盖性地创建补间动画）
		private static void CreateTweens(KeyPoint keyPoint)
		{
			if (keyPoint.nextPoint == null)
				return;
			DeleteTweens(keyPoint);

			double fromTime = keyPoint.millseconds;
			double toTime = keyPoint.nextPoint.millseconds;
			double deltaTime = toTime - fromTime;

			double fromValue = double.Parse(keyPoint.value);
			double toValue = double.Parse(keyPoint.nextPoint.value);
			double deltaValue = toValue - fromValue;

			IList<KeyPoint> rootTweens = keyPoint.tweenList;

			for (int i = 1; i < TWEENS_MIN_COUNT; i++)
			{
				double rawT = i / TWEENS_MIN_COUNT;
				double rawX = Bezier.ThreeCube(rawT, 0d, keyPoint.smoothIn.X, keyPoint.smoothOut.X, 1d);
				double rawY = Bezier.ThreeCube(rawT, 0d, keyPoint.smoothIn.Y, keyPoint.smoothOut.Y, 1d);
				int newtime = (int)(fromTime + deltaTime * rawX);

				string newValue = (fromValue + rawY * deltaValue).ToString();

				KeyPoint newtween = KeyPoint.AddTweenPoint(keyPoint, newtime, newValue);
				rootTweens.Add(newtween);
			}
		}
		#endregion

		#region delete

		//删除帧点，以及它的补间与骨骼同步点
		[Obsolete("过时的方案", true)]
		public static void DeleteKeyPoint_Obsolete(KeyPoint keyPoint)
		{
			if (keyPoint.isLocked == true)
				BugLog.ThrowError(new KeyPointLockProtectedException());

			KeyPoint prevp = null, nextp = null;
			ObservableCollection<KeyPoint> pointList = null;//上级点集中的点集集合

			//空/实图层 + 补间状态处理
			//常规情况：空，非补间
			if (keyPoint.IsEmptyPoint == true && keyPoint.IsTween == false)
			{
				pointList = keyPoint.points.Points;
				pointList.Remove(keyPoint);
				keyPoint.pptPoint = null;
			}
			//最常规情况：非空非补帧点
			else if (keyPoint.IsEmptyPoint == false && keyPoint.IsTween == false)
			{
				pointList = keyPoint.points.Points;
				pointList.Remove(keyPoint);
				keyPoint.pptPoint.Delete();
				keyPoint.pptPoint = null;
			}
			else if (keyPoint.IsEmptyPoint == true && keyPoint.IsTween == true)
			{
				//NOTHING
			}
			else if (keyPoint.IsEmptyPoint == false && keyPoint.IsTween == true)
			{
				keyPoint.pptPoint.Delete();
				keyPoint.pptPoint = null;
			}

			//补间帧点不存在前驱与后驱帧点，因为前驱和后驱点都是可见点，补间点是隐藏的。
			if (keyPoint.IsTween == false)
			{
				//正常帧点需要重新建立伪链表连接
				prevp = keyPoint.previousPoint;
				nextp = keyPoint.nextPoint;
				if (prevp != null) prevp.nextPoint = nextp;
				if (nextp != null) nextp.previousPoint = prevp;
			}

			List<KeyPoint> childPoints = keyPoint.childPointList;
			List<KeyPoint> tweenPoints = keyPoint.tweenList;

			//骨骼节点同步
			foreach (KeyPoint childPoint in childPoints)
			{
				DeleteKeyPoint(childPoint);
			}
			childPoints.Clear();

			//补间节点同步
			foreach (KeyPoint tweenPoint in tweenPoints)
			{
				DeleteKeyPoint(tweenPoint);
			}
			tweenPoints.Clear();
		}


		//删除帧点，以及它的补间与骨骼同步点
		public static void DeleteKeyPoint(KeyPoint keyPoint)
		{
			//删除部分，我们首先要对主点进行删除
			//存在着两种类型的主点：空主点与非空主点
			//抽象的讲，存在着四种类型的帧点，独立点，骨骼点，补间点，骨骼补间点
			//这几种都需要另外讨论。

			KeyPoint.DeleteSingleTweens(keyPoint);

			KeyPoint.DeleteSingleKeyPoint(keyPoint);

			foreach (KeyPoint childpoint in keyPoint.childPointList)
			{
				KeyPoint.DeleteKeyPoint(childpoint);
			}

			keyPoint.childPointList.Clear();

		}

		public static void DeleteTweens(KeyPoint keyPoint)
		{
			KeyPoint.DeleteSingleTweens(keyPoint);
			foreach (KeyPoint childpoint in keyPoint.childPointList)
			{
				KeyPoint.DeleteTweens(childpoint);
			}
		}

		//删除单个独立帧点，并将链表重新连接/骨骼帧点随着父层的消失而消失，骨骼序列虽然不可见，但是仍然具有实体
		private static void DeleteSingleKeyPoint(KeyPoint keyPoint)
		{
			ICollection<KeyPoint> pointList = keyPoint.points.Points;

			pointList.Remove(keyPoint);//独立点将会在序列轴中显示出来，其他点不会

			if (keyPoint.IsEmptyPoint == false)//同时删除实体点（重要）
				keyPoint.pptPoint.Delete();

			keyPoint.pptPoint = null;
			keyPoint.parentPoint = null;

			//重新建立连接
			KeyPoint prevp = null, nextp = null;
			if (keyPoint.previousPoint != null) prevp = keyPoint.previousPoint;
			if (keyPoint.nextPoint != null) nextp = keyPoint.nextPoint;

			if (prevp != null) prevp.nextPoint = nextp;
			if (nextp != null) nextp.previousPoint = prevp;


		}

		//补间点在任何轴中都不可见
		private static void DeleteSingleTweenKeyPoint(KeyPoint keyPoint)
		{
			if (keyPoint.IsEmptyPoint == false)//同时删除实体点（重要）
				keyPoint.pptPoint.Delete();
			keyPoint.pptPoint = null;
			keyPoint.tweenParent = null;
		}

		//删除单个帧点的两端补间
		private static void DeleteSingleTweens(KeyPoint keyPoint)
		{
			IList<KeyPoint> prvTweens, cntTweens;//前后帧的补间集合。
			if (keyPoint.previousPoint != null)
				prvTweens = keyPoint.previousPoint.tweenList;
			else
				prvTweens = new List<KeyPoint>();

			if (keyPoint.nextPoint != null)
				cntTweens = keyPoint.tweenList;
			else
				cntTweens = new List<KeyPoint>();

			foreach (KeyPoint ptween in prvTweens)
			{
				KeyPoint.DeleteSingleTweenKeyPoint(ptween);
			}

			foreach (KeyPoint ctween in cntTweens)
			{
				KeyPoint.DeleteSingleTweenKeyPoint(ctween);
			}
			prvTweens.Clear();
			cntTweens.Clear();
		}

		#endregion

		#region add

		//仅添加，不插入
		private static KeyPoint CreateEmptyPoint
			(KeyPoints points, int millseconds, string value, string pptFormula)
		{
			KeyPoint keyPoint = null;
			if (points.IsEmptyPoints)
			{
				keyPoint = new KeyPoint
					(points, null, null, null, millseconds, false);
			}
			//非空图层需要在PPT中注册对应的帧点
			else
			{
				float pptTimePrecent = KeyPoints.GetPptTimePrecent(points, millseconds);
				ppt.AnimationPoints pptPoints = points.pptPoints;

				ppt.AnimationPoint newpoint = pptPoints.Add();
				newpoint.Time = pptTimePrecent;
				newpoint.Value = value;
				newpoint.Formula = pptFormula;

				keyPoint = new KeyPoint
					(points, newpoint, null, null, millseconds, false);
			}


			return keyPoint;

		}

		private static KeyPoint CreateEmptyPoint(
			KeyPoints points,
			int millseconds,
			KeyPoint prevPoint, KeyPoint nextPoint,
			out float pptTimePrecent, string value, string pptFormula, out int newPosInPptPoints)
		{
			pptTimePrecent = 0f;
			newPosInPptPoints = 1;
			KeyPoint keyPoint = null;
			if (points.IsEmptyPoints)
			{
				keyPoint = new KeyPoint
					(points, null, prevPoint, nextPoint, millseconds, false);
			}
			//非空图层需要在PPT中注册对应的帧点
			else
			{
				pptTimePrecent = KeyPoints.GetPptTimePrecent(points, millseconds);
				ppt.AnimationPoints pptPoints = points.pptPoints;

				KeyPoints.TryAddInPoints(points, pptTimePrecent, out newPosInPptPoints);

				ppt.AnimationPoint newpoint = pptPoints.Add(newPosInPptPoints);
				newpoint.Time = pptTimePrecent;
				newpoint.Value = value;
				newpoint.Formula = pptFormula;

				keyPoint = new KeyPoint
					(points, newpoint, prevPoint, nextPoint, millseconds, false);
			}

			if (prevPoint != null) prevPoint.nextPoint = keyPoint;
			if (nextPoint != null) nextPoint.previousPoint = keyPoint;

			return keyPoint;

		}

		//添加普通点
		internal static KeyPoint AddNormalPoint(KeyPoints points, int millseconds)
		{
			//非补间动画需要进行格式化
			millseconds.MillSecondslize();

			int newPosInPptPoints = 1;
			float pptTimePrecent = 0f;
			string value = "0";
			string pptFormula = KeyPoint.CreateAdjustedValue(points, "$");

			KeyPoint keyPoint = null, prevPoint = null, nextPoint = null;

			ObservableCollection<KeyPoint> pointList = points.Points;

			int newPosInList = 0;

			#region 异常检查
			if (KeyPoints.IsInDurationRange(points, millseconds) == false)
				BugLog.ThrowError(new KeyPointsNewTimeOutOfRangeException());

			if (KeyPoints.TryAddInPoints(points, millseconds, out newPosInList) == false)
				BugLog.ThrowError(new KeyPointsKeyPointReloadException(millseconds));
			#endregion

			//补间动画不具有前后联系，建立关系
			if (newPosInList - 1 >= 0) prevPoint = pointList[newPosInList - 1];
			if (newPosInList < pointList.Count) nextPoint = pointList[newPosInList];

			keyPoint = KeyPoint.CreateEmptyPoint(
				points, millseconds, prevPoint, nextPoint,
				out pptTimePrecent, value, pptFormula, out newPosInPptPoints);

			//非补间动画需要在List中注册，且建立重新连接
			points.UIDispatcher.Invoke(() =>
			{
				pointList.Insert(newPosInList, keyPoint);//注册在点集合里
			});

			List<KeyPoint> childPointList = keyPoint.childPointList;

			foreach (KeyPoints childPoints in points.childPointsList)
			{
				KeyPoint newChildPoint = KeyPoint.AddNormalPointBinded
					(childPoints, newPosInList, newPosInPptPoints, pptTimePrecent, value, pptFormula);
				newChildPoint.parentPoint = keyPoint;
				childPointList.Add(newChildPoint);
			}

			return keyPoint;
		}

		//绑定点的传播
		private static KeyPoint AddNormalPointBinded
			(KeyPoints points, int newPosInList, int newPosInPptPoints, float pptTimePrecent, string value, string pptFormula)
		{
			KeyPoint keyPoint = null;

			//补间动画和依赖不具有前后联系

			if (points.IsEmptyPoints)
			{
				keyPoint = new KeyPoint
					(points, null, null, null, 0, false);
			}
			//非空图层需要在PPT中注册对应的帧点
			else
			{
				ppt.AnimationPoints pptPoints = points.pptPoints;
				ppt.AnimationPoint newpoint = pptPoints.Add(newPosInPptPoints);
				newpoint.Time = pptTimePrecent;
				newpoint.Value = value;
				newpoint.Formula = pptFormula;

				keyPoint = new KeyPoint
					(points, newpoint, null, null, 0, false);
			}

			//非补间动画需要在List中注册，且建立重新连接
			points.UIDispatcher.Invoke(() =>
			{
				points.Points.Insert(newPosInList, keyPoint);//注册在点集合里
			});

			List<KeyPoint> childPointList = keyPoint.childPointList;

			foreach (KeyPoints childPoints in points.childPointsList)
			{
				KeyPoint newChildPoint = KeyPoint.AddNormalPointBinded
					(childPoints, newPosInList, newPosInPptPoints, pptTimePrecent, value, pptFormula);
				newChildPoint.parentPoint = keyPoint;
				childPointList.Add(newChildPoint);
			}

			return keyPoint;
		}

		//添加锁定点
		internal static KeyPoint AddLockedPoint(KeyPoints points, bool isHead = true)
		{
			//非补间动画需要进行格式化
			int millseconds = 0;

			if (isHead)
				millseconds = points.behavior.effect.delayTime;
			else
				millseconds = points.behavior.effect.delayTime + points.behavior.effect.duration;

			KeyPoint keyPoint = null, prevPoint = null, nextPoint = null;

			ICollection<KeyPoint> pointList = points.Points;

			//空图层不需要对PPT动画做出任何更改
			if (points.IsEmptyPoints)
			{
				keyPoint = new KeyPoint
					(points, null, prevPoint, nextPoint, millseconds, true);
			}
			//非空图层需要在PPT中注册对应的帧点
			else
			{
				ppt.AnimationPoints pptPoints = points.pptPoints;
				int newPosInPptPoints = pptPoints.Count + 1;
				ppt.AnimationPoint newpoint = pptPoints.Add(newPosInPptPoints);
				newpoint.Formula = KeyPoint.CreateAdjustedValue(points, "$");

				keyPoint = new KeyPoint
					(points, newpoint, prevPoint, nextPoint, millseconds, true);
			}

			//非补间动画需要在List中注册，且建立重新连接
			points.UIDispatcher.Invoke(() =>
			{
				pointList.Add(keyPoint);//注册在点集合里
			});

			return keyPoint;
		}

		//添加补间点
		private static KeyPoint AddTweenPoint(KeyPoint tweenParent, int millseconds, string value)
		{
			KeyPoint keyPoint = null;
			KeyPoints points = tweenParent.points;
			ICollection<KeyPoint> pointList = points.Points;
			ppt.AnimationPoints pptPoints = points.pptPoints;

			int newPosInList = 0;
			float pptTimePrecent = 0f;
			int newPosInPptPoints = 1;
			KeyPoints.TryAddInPoints(points, millseconds, out newPosInList);

			keyPoint = CreateEmptyPoint
				(points, millseconds,null,null,out pptTimePrecent, value, "$",out newPosInPptPoints) ;

			keyPoint.isTween = true;

			//建立补间的父子关系
			tweenParent.tweenList.Add(keyPoint);
			keyPoint.tweenParent = tweenParent;

			List<KeyPoint> childPointList = keyPoint.childPointList;

			foreach (KeyPoints childPoints in points.childPointsList)
			{
				KeyPoint newChildPoint = KeyPoint.AddNormalPointBinded
					(childPoints, newPosInList, newPosInPptPoints, pptTimePrecent, value, "$");
				newChildPoint.parentPoint = keyPoint;
				childPointList.Add(newChildPoint);
			}

			return keyPoint;
		}


		//拷贝帧点（到指定图层）
		internal static KeyPoint CopyKeyPoint(KeyPoint keyPoint, KeyPoints keyPoints, int millseconds)
		{
			KeyPoint newKeyPoint = KeyPoint.AddNormalPoint(keyPoints, millseconds);
			newKeyPoint.value = keyPoint.value;
			newKeyPoint.formula = keyPoint.formula;

			if (newKeyPoint.IsEmptyPoint == false)
			{
				newKeyPoint.value = keyPoint.pptPoint.Value;
				newKeyPoint.formula = keyPoint.pptPoint.Formula;
			}

			newKeyPoint.script = new Script(newKeyPoint.Script);
			newKeyPoint.smoothIn = new SmoothPoint(newKeyPoint.smoothIn);
			newKeyPoint.smoothOut = new SmoothPoint(newKeyPoint.smoothOut);

			foreach (KeyPoint childPoint in keyPoint.childPointList)
			{
				KeyPoint.CopyKeyPointBinded(keyPoint, keyPoint.value, keyPoint.formula);
			}
			return newKeyPoint;
		}

		private static void CopyKeyPointBinded(KeyPoint keyPoint, string value, string pptformula)
		{
			if (keyPoint.IsEmptyPoint == false)
			{
				keyPoint.pptPoint.Value = value;
				keyPoint.pptPoint.Value = pptformula;
			}

			foreach (KeyPoint childPoint in keyPoint.childPointList)
			{
				KeyPoint.CopyKeyPointBinded(keyPoint, value, pptformula);
			}
			//CreateTweens(newKeyPoint);
		}

		internal static void CreateBindedKeyPoint(KeyPoint rootPoint, KeyPoints keyPoints)
		{
			KeyPoint newKeyPoint = KeyPoint.CreateEmptyPoint
				(keyPoints, rootPoint.millseconds, rootPoint.value, rootPoint.formula);

			rootPoint.childPointList.Add(newKeyPoint);
			newKeyPoint.parentPoint = rootPoint;
		}

		#endregion

		#endregion

	}
}
