﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ppt = Microsoft.Office.Interop.PowerPoint;

using FanBox.FanBoxLib.Exceptions;

namespace FanBox.FanBoxLib.Models
{
	internal static class KeyPointsBugLog
	{
		public static void AddKeyPoint
			(KeyPoints keyPoints, int millseconds, string value, string formula)
		{
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointAddKeyPoint,
				effect.shape.Name,
				behavior.Name,
				millseconds,
				value,
				formula);
			//图形{0}属性{1}，添加帧时{2}，帧值{3}，帧表达式{4}
			BugLog.Add(output);
		}

		[Obsolete("删除具体的业务在Point内部，不需要提前在Points里设置")]
		public static void DeleteKeyPoint(KeyPoints keyPoints, KeyPoint keyPoint)
		{
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointsDeletePointKey,
				effect.shape.Name,
				behavior.Name,
				keyPoint.millseconds);
			//图形{0}属性{1}位于{2}秒关键帧，删除。
			BugLog.Add(output);
		}

		public static void ClearAll(KeyPoints keyPoints)
		{
			Behavior behavior = keyPoints.behavior;
			Effect effect = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.KeyPointsClearAll,
				effect.shape.Name,
				behavior.Name);
			//图形{0}属性{1}清除所有帧点。
			BugLog.Add(output);
		}
	}

	internal class KeyPoints : INotifyPropertyChanged
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal Behavior behavior;
		internal ppt.AnimationPoints pptPoints;
		private ObservableCollection<KeyPoint> pointList;

		private KeyPoints parentPoints;//父点集合
		internal List<KeyPoints> childPointsList;//子点集合列表
		#endregion

		#region public properties
		internal Dispatcher UIDispatcher
		{
			get { return this.behavior.UIDispatcher; }
		}

		public KeyPoints ParentPoints
		{
			get { return this.parentPoints; }
		}

		internal bool IsEmptyPoints
		{
			get { return this.behavior.IsEmptyBehavior; }
		}

		//是否为骨骼独立帧点序列
		public bool IsIndividual
		{
			get { return this.behavior.IsIndividual; }
		}

		public ObservableCollection<KeyPoint> Points
		{
			get { return pointList; }
		}



		#endregion

		#region ctors
		internal KeyPoints(Behavior behavior, ppt.AnimationPoints points)
		{
			this.pointList = new ObservableCollection<KeyPoint>();
			this.childPointsList = new List<KeyPoints>();
			this.behavior = behavior;
			this.pptPoints = points;
		}
		#endregion

		#region public methods

		public KeyPoint AddPoint(int millsecond, string value)
		{
			return this.AddPoint(millsecond, value, "$");
		}

		public KeyPoint AddPoint(int millsecond, string value, string formula)
		{
			return this.AddPoint(millsecond, value, formula, false);
		}

		public KeyPoint AddPoint
			(int millsecond, string value, string formula, bool isLocked)
		{
			//TODO KeyPointsBugLog.AddKeyPoint(this, time, value, formula);

			if (KeyPoints.IsInDurationRange(this, millsecond) == false && isLocked == false)
				BugLog.ThrowError(new KeyPointsNewTimeOutOfRangeException());

			KeyPoint keyPoint = KeyPoint.AddNormalPoint(this, millsecond);

			return keyPoint;
		}

		public void DeletePoint(KeyPoint keyPoint)
		{
			keyPoint.Delete();
		}

		public void Clear()
		{
			KeyPointsBugLog.ClearAll(this);

			List<KeyPoint> pointArr = new List<KeyPoint>(this.pointList);

			foreach (KeyPoint keyPoint in pointArr)
			{
				keyPoint.Value = this.behavior.Value.ToString();
				if (keyPoint.IsLocked == false)
					keyPoint.Delete();
			}

			this.pointList.Clear();
		}

		#endregion

		#region private methods
		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region static methods

		public static bool IsInDurationRange(KeyPoints points, int millseconds)
		{
			Effect eff = points.behavior.effect;
			int begintime = eff.delayTime;
			int totaltime = eff.delayTime + eff.duration;
			return millseconds > begintime && millseconds < totaltime;
		}


		//尝试在Points中加入指定时间值的点，并返回可插入点的位置
		internal static bool TryAddInPoints(KeyPoints points, float time, out int pos)
		{
			pos = 1;
			foreach (ppt.AnimationPoint p in points.pptPoints)
			{
				if (time < p.Time) break;
				pos++;
			}
			return true;
		}

		//获得PPT的相对比例时间，也就是获得调教完的时间
		internal static float GetPptTimePrecent(KeyPoints points, int millseconds)
		{
			Effect eff = points.behavior.effect;
			double delay = eff.delayTime;
			double dur = eff.duration;
			return (float)((1.0 * millseconds - delay) / dur);
		}

		//将所有帧点向指定序列层进行引用
		internal static void CreateBindedKeyPoints(KeyPoints sourceKeyPoints, KeyPoints targetKeyPoints)
		{
			foreach (KeyPoint keyPoint in sourceKeyPoints.Points)
			{
				KeyPoint.CreateBindedKeyPoint(keyPoint, targetKeyPoints);
			}

			sourceKeyPoints.childPointsList.Add(targetKeyPoints);
			targetKeyPoints.parentPoints = sourceKeyPoints;
		}

		
		#endregion

	}
}
