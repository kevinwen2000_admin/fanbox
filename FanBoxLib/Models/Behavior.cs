﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Threading;
using ppt = Microsoft.Office.Interop.PowerPoint;

using FanBox.FanBoxLib.Exceptions;
using FanBox.FanBoxLib.Tools;

namespace FanBox.FanBoxLib.Models
{
	internal class BehaviorBugLog
	{
		public static void BindBone(Behavior behavior, Effect targetEffect)
		{
			Effect eff = behavior.effect;
			string output = string.Format
				(Resouces.LogMessages.BehaviorBindBone,
				eff.shape.Name,
				behavior.Name,
				targetEffect.shape.Name);
			BugLog.Add(output);
		}
	}

	public class Behavior : INotifyPropertyChanged
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		private string name;//行为的名称
		internal ppt.AnimationBehavior pptBehavior;//对应ppt的行为
		internal ppt.PropertyEffect pptPropertyEffect;//对应ppt的属性
		internal Effect effect;//父效果
		private Behavior parentBehavoir;//父行为
		private List<Behavior> childBehaviorList;//子效果
		internal KeyPoints points;//帧点集合，用于管理帧点

		private double realValue;//实际点参数

		internal EnumProperty property;//属性类型

		internal KeyPoint headPoint, rearPoint;

		//private double duration;
		//private double delayTime; 

		#endregion

		#region public properties
		internal Dispatcher UIDispatcher
		{
			get { return this.effect.UIDispatcher; }
		}

		public string Name
		{
			get => this.name;
		}

		public Behavior ParentBehavior
		{
			get => this.parentBehavoir;
		}

		public IEnumerable<Behavior> ChildBehaviors
		{
			get => this.childBehaviorList;
		}

		public ObservableCollection<KeyPoint> Points
		{
			get => this.points.Points;
		}

		public bool IsEmptyBehavior { get => this.effect.IsEmptyEffect; }

		public bool IsIndividual
		{
			get
			{
				if (this.effect.IsIndividual == false)
					return false;
				else
					return this.effect.IsIndividual && this.parentBehavoir == null;
			}
		}

		public double Value
		{
			get => Behavior.GetValue(this);
			set => Behavior.SetBehavior(this, value);
		}

		#endregion

		#region ctors
		//构造函数
		internal Behavior(ppt.AnimationBehavior behavior, Effect pEffect, EnumProperty property)
		{
			this.pptBehavior = behavior;
			this.effect = pEffect;

			this.property = property;

			this.realValue = 0d;

			this.parentBehavoir = null;
			this.childBehaviorList = new List<Behavior>();

			this.points = null;
			this.pptPropertyEffect = null;

			if (behavior != null)
			{
				this.points = new KeyPoints(this, behavior.PropertyEffect.Points);
				this.pptPropertyEffect = behavior.PropertyEffect;
				this.pptPropertyEffect.From = 0f;
				this.pptPropertyEffect.To = 0f;
			}
			else
			{
				this.points = new KeyPoints(this, null);
			}

		}
		#endregion

		#region public methods

		public KeyPoint AddKeyPoint(int millseconds)
		{
			return Behavior.AddNormalPoint(this, millseconds);
		}

		public void DeletePoint(KeyPoint keyPoint)
		{
			this.points.DeletePoint(keyPoint);
		}

		public void Delete()
		{
			Behavior.DeleteBehavior(this);
		}

		public void Preset()
		{
			Behavior.Preset(this);
		}

		public void Preview(int millsecond)
		{
			Behavior.Preview(this, millsecond);
		}

		public override string ToString()
		{
			string nl = $"{Environment.NewLine}";
			string head = $"[Behavior] name:{this.name}";
			string pointsStr = $"keypoint_count:{this.Points.Count()}";
			return $"{head}\t{pointsStr}";
		}

		#endregion

		#region private methods
		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region static methods
		internal bool IsInDurationRange(Behavior behavior, int millseconds)
		{
			return Effect.IsInDurationRange(behavior.effect, millseconds);
		}

		//添加自由帧
		private static KeyPoint AddNormalPoint(Behavior behavior, int millseconds)
		{
			return KeyPoint.AddNormalPoint(behavior.points, millseconds);
		}
		//添加预设的锁定帧
		private static KeyPoint AddLockedPoint(Behavior behavior, bool isHead)
		{
			return KeyPoint.AddLockedPoint(behavior.points, isHead);
		}

		//创建空行为，不建立任何关联。
		private static Behavior CreateEmptyBehavior(Effect effect, EnumProperty property,
			ppt.MsoAnimAdditive additive)
		{
			Behavior newbhv = null;

			//实体效果的创建
			if (effect.IsEmptyEffect)
			{
				newbhv = new Behavior(null, effect, property);
			}
			else
			{
				ppt.AnimationBehavior bhv = null;
				ppt.Effect eff = effect.pptEffect;
				bhv = eff.Behaviors.Add(ppt.MsoAnimType.msoAnimTypeProperty);
				bhv.Timing.Duration = eff.Timing.Duration;
				bhv.Additive = additive;
				bhv.PropertyEffect.Property = PropertyConverter.Convert(property);
				bhv.Timing.Duration = effect.pptEffect.Timing.Duration;
				newbhv = new Behavior(bhv, effect, property);
			}
			return newbhv;
		}

		//向指定额外区创建一个具有连接性的非注册行为
		internal static Behavior CreateBindedBehavior(Behavior sourceBehavior, Effect targetChildEffect, EnumProperty property)
		{
			//建立
			Behavior newbhv = Behavior.CreateEmptyBehavior(
				targetChildEffect,
				property,
				ppt.MsoAnimAdditive.msoAnimAdditiveAddSum);

			KeyPoints.CreateBindedKeyPoints(sourceBehavior.points, newbhv.points);

			sourceBehavior.childBehaviorList.Add(newbhv);
			newbhv.parentBehavoir = sourceBehavior;

			return newbhv;
		}

		//向指定Effect注册一条无任何信息无任何上下级关系与注册的动画行为
		//甚至没有默认的帧点，具有叠加的性质
		internal static void AddEmptyBehavior(Effect effect, string bhvName, EnumProperty property)
		{
			if (effect.IsIndividual == false) return;

			Behavior newbhv = Behavior.CreateEmptyBehavior
				(effect, property, ppt.MsoAnimAdditive.msoAnimAdditiveAddSum);

			effect.UIDispatcher.Invoke(() => { effect.animBehaviorList.Add(newbhv); });

			effect.animBehaviorList.Add(newbhv);
		}

		//增加属性行为，其实和空行为别无二致，但是在注册会注册到效果属性集合中
		//属性行为在拷贝层中是不存在的。
		internal static Behavior AddPropertyBehavior(Effect effect, EnumProperty property)
		{
			Behavior newbhv = Behavior.CreateEmptyBehavior
				(effect, property, ppt.MsoAnimAdditive.msoAnimAdditiveAddBase);

			effect.propBehaviorList.Add(newbhv);
			effect.shape.propertyDic[property].propBehavior = newbhv;
			return newbhv;
		}

		//增加标准行为
		internal static Behavior AddStandardBehavior(Effect effect, EnumProperty property)
		{
			Behavior newbhv = Behavior.CreateEmptyBehavior
				(effect, property, ppt.MsoAnimAdditive.msoAnimAdditiveAddSum);

			effect.UIDispatcher.Invoke(() => { effect.animBehaviorList.Add(newbhv); });

			effect.animBehaviorList.Add(newbhv);

			newbhv.rearPoint = Behavior.AddLockedPoint(newbhv, true);
			newbhv.headPoint = Behavior.AddLockedPoint(newbhv, false);
			effect.shape.propertyDic[property].animBehavior = newbhv;

			return newbhv;
		}

		internal static void DeleteBehavior(Behavior behavior)
		{
			var behaviorList = behavior.effect.animBehaviorList;
			var behaviorsDic = behavior.effect.propBehaviorList;
			var childBehaviors = behavior.childBehaviorList;

			behaviorList.Remove(behavior);

			foreach (Behavior childBehavior in childBehaviors)
			{
				Behavior.DeleteBehavior(childBehavior);
			}
			behavior.pptBehavior.Delete();

			behavior.effect = null;
		}

		internal static void Preset(Behavior behavior)
		{
			if (behavior.IsIndividual == false)
				return;
			Shape shp = behavior.effect.shape;

			if (shp.isDummy == false)
			{
				switch (behavior.property)
				{
					case EnumProperty.X:
						Shape.PresetX(shp, behavior.realValue);
						break;
					case EnumProperty.Y:
						Shape.PresetY(shp, behavior.realValue);
						break;
					case EnumProperty.Width:
						Shape.PresetWidth(shp, behavior.realValue);
						break;
					case EnumProperty.Height:
						Shape.PresetHeight(shp, behavior.realValue);
						break;
					//case ppt.MsoAnimProperty.msoAnimRotation:
					//	break;
					default:
						break;
				}
			}


			foreach (Behavior childBehvaior in behavior.childBehaviorList)
			{
				Behavior.PresetBinded(childBehvaior);
			}

		}

		private static void PresetBinded(Behavior behavior)
		{
			Shape shp = behavior.effect.shape;
			if (shp.isDummy == false)
			{
				switch (behavior.property)
				{
					case EnumProperty.X:
						Shape.PresetX(shp, behavior.realValue);
						break;
					case EnumProperty.Y:
						Shape.PresetY(shp, behavior.realValue);
						break;
					case EnumProperty.Width:
						Shape.PresetWidth(shp, behavior.realValue);
						break;
					case EnumProperty.Height:
						Shape.PresetHeight(shp, behavior.realValue);
						break;
					//case ppt.MsoAnimProperty.msoAnimRotation:
					//	break;
					default:
						break;
				}
			}
			foreach (Behavior childBehvaior in behavior.childBehaviorList)
			{
				Behavior.PresetBinded(childBehvaior);
			}

		}

		internal static void Preview(Behavior behavior, int millsecond)
		{
			if (behavior.IsIndividual == false)
				return;
			Shape shp = behavior.effect.shape;
			int currentTime = millsecond.ToMillSeconds();

			KeyPoint currentPoint = null, nextPoint = behavior.rearPoint;
			foreach (KeyPoint point in behavior.Points)
			{
				if (point.millseconds > currentTime)
				{
					nextPoint = point;
					break;
				}
			}

			int startTime = 0, endTime = 0;
			double startValue = 0d, endValue = 0d;
			double currentValue = 0d;
			double currentFormula = 0d;

			currentPoint = nextPoint.previousPoint;
			startTime = currentPoint.millseconds;
			endTime = nextPoint.millseconds;

			startValue = FormulaAnalyzer.Calculate(currentPoint.valueElems, 0);
			endValue = FormulaAnalyzer.Calculate(nextPoint.valueElems, 0);

			double ctime = currentTime - startTime;
			double dtime = endTime - startTime;
			double dvalue = endValue - startValue;

			currentValue = dvalue * ctime / dtime + startValue;
			currentFormula = FormulaAnalyzer.Calculate(currentPoint.formulaElems, currentValue);
			if (shp.isDummy == false)
			{
				switch (behavior.property)
				{
					case EnumProperty.X:
						Shape.AddX(shp, currentFormula);
						break;
					case EnumProperty.Y:
						Shape.AddY(shp, currentFormula);
						break;
					case EnumProperty.Width:
						Shape.AddWidth(shp, currentFormula);
						break;
					case EnumProperty.Height:
						Shape.AddHeight(shp, currentFormula);
						break;
					//case ppt.MsoAnimProperty.msoAnimRotation:
					//	break;
					default:
						break;
				}
			}
			foreach (Behavior childBehvaior in behavior.childBehaviorList)
			{
				Behavior.PreviewBinded(childBehvaior, currentFormula);
			}
		}

		private static void PreviewBinded(Behavior behavior, double formulaResult)
		{
			Shape shp = behavior.effect.shape;
			if (shp.isDummy == false)
			{
				switch (behavior.property)
				{
					case EnumProperty.X:
						Shape.AddX(shp, formulaResult);
						break;
					case EnumProperty.Y:
						Shape.AddY(shp, formulaResult);
						break;
					case EnumProperty.Width:
						Shape.AddWidth(shp, formulaResult);
						break;
					case EnumProperty.Height:
						Shape.AddHeight(shp, formulaResult);
						break;
					//case ppt.MsoAnimProperty.msoAnimRotation:
					//	break;
					default:
						break;
				}
			}
			foreach (Behavior childBehvaior in behavior.childBehaviorList)
			{
				Behavior.PreviewBinded(childBehvaior, formulaResult);
			}

		}

		private static double GetValue(Behavior behavior)
		{
			if (behavior.IsIndividual)
				return behavior.realValue;
			else
				return Behavior.GetValue(behavior.parentBehavoir);
		}

		private static void SetValue(Behavior behavior, double value)
		{
			if (behavior.IsIndividual == false)
				return;

			behavior.realValue = value;
			double pvalue = 1.0d * value / Screen.GetProperty(behavior.property);

			if (behavior.IsEmptyBehavior == false)
			{
				behavior.pptPropertyEffect.From = pvalue;
				behavior.pptPropertyEffect.To = pvalue;
			}
			behavior.PropertyChangedInvoke("Value");
			foreach (Behavior childBehavior in behavior.childBehaviorList)
			{
				Behavior.SetValueBinded(behavior, pvalue);
			}
		}

		private static void SetValueBinded(Behavior behavior, double pvalue)
		{
			if (behavior.IsEmptyBehavior == false)
			{
				behavior.pptPropertyEffect.From = pvalue;
				behavior.pptPropertyEffect.To = pvalue;
			}
			foreach (Behavior childBehavior in behavior.childBehaviorList)
			{
				Behavior.SetValueBinded(behavior, pvalue);
			}
		}

		internal static void SetBehavior(Behavior behavior, double value)
		{
			Behavior.SetValue(behavior, value);
		}

		#endregion
	}
}
