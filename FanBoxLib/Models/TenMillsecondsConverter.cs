﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Models
{
	//我们认为PPT的秒数精度单位为0.01。如果再小一点，那就没有什么意义了。
	internal static class Int32
	{

		/// <summary>
		/// 将指定毫秒转换成FanBox允许的格式，也就是10ms作为基础的毫秒格式，精度为10^1
		/// </summary>
		/// <param name="ms">原毫秒</param>
		/// <returns>FanBox允许的秒精度格式</returns>
		public static float ToSeconds(this int ms)
		{
			return ToMillSeconds(ms) / 1000.0f;
		}

		/// <summary>
		/// 将指定毫秒转换成FanBox允许的格式，也就是10ms作为基础的毫秒格式，精度为10^1
		/// </summary>
		/// <param name="ms">原毫秒</param>
		/// <returns>FanBox允许的毫秒精度格式</returns>
		public static int ToMillSeconds(this int ms)
		{
			 return ms - ms % 10;
		}

		/// <summary>
		/// 毫秒化
		/// </summary>
		/// <param name="ms">毫秒</param>
		public static void MillSecondslize(ref this int ms)
		{
			ms = ms - ms % 10;
		}

	}

	
}
