﻿using System;
using System.Collections.Generic;
using ppt = Microsoft.Office.Interop.PowerPoint;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using FanBox.FanBoxLib.Exceptions;
using System.Collections;

namespace FanBox.FanBoxLib.Models
{
	public class Shapes : INotifyPropertyChanged, IEnumerable<Shape>
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal Slide parentSlide;
		internal ppt.Shapes pptShapes;
		internal ObservableCollection<Shape> shapeList;
		internal Dictionary<int, Shape> shapeDic;

		#endregion

		#region public properties
		public Dispatcher UIDispatcher
		{
			get => this.parentSlide.uiDispatcher;
		}

		public ObservableCollection<Shape> ShapesList
		{
			get { return this.shapeList; }
		}

		public Slide ParentSlide
		{
			get { return this.parentSlide; }
		}
		#endregion

		#region ctors
		internal Shapes(ppt.Shapes shapes, Slide slide)
		{
			this.pptShapes = shapes;
			this.parentSlide = slide;
			this.shapeList = new ObservableCollection<Shape>();
			this.shapeDic = new Dictionary<int, Shape>();
		}
		#endregion

		#region indexer
		public Shape this [string name]
		{
			get
			{
				return this.shapeDic[pptShapes[name].Id];
			}
		}

		public Shape this[ppt.Shape shape]
		{
			get
			{
				return this.shapeDic[shape.Id];
			}
		}

		public Shape this[int id]
		{
			get
			{
				return this.shapeDic[id];
			}
		}

		#endregion

		#region public methods



		public IEnumerator<Shape> GetEnumerator()
		{
			return shapeDic.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return shapeDic.Values.GetEnumerator();
		}

		#endregion

		#region private methods

		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region static methods

		//是否在图形集合中
		private static bool IsInShapes(Shapes shapes, string name)
		{
			foreach (ppt.Shape shp in shapes.pptShapes)
			{
				if (shp.Name == name)
					return true;
			}
			return false;
		}

		private static void Preview(Shapes shapes, int millseconds)
		{
			foreach (Shape shape in shapes)
			{
				Shape.Preset(shape);
			}

			foreach (Shape shape in shapes)
			{
				Shape.Preview(shape, millseconds);
			}

			foreach (Shape shape in shapes)
			{
				Shape.PreviewPosition(shape);
			}
		}


		//向图形图层表注册形状
		private static void AddNormalShape(Shapes shapes, ppt.Shape pptshp)
		{
			int shpid = pptshp.Id;
			if (shapes.shapeDic.ContainsKey(shpid))
				return;
			Shape.AddNormalShape(shapes, pptshp);
		}

		//向图层集合注册空图层
		private static void AddEmptyShape(Shapes shapes)
		{
			Shape.AddEmptyShape(shapes);
		}

		private static void DeleteShape(Shapes shapes, Shape shape)
		{
			//TODO Shape.ClearAllEffects(shape);
			if (shape.isDummy == true)
				shapes.shapeDic.Remove(shape.id);
		}

		#endregion
	}
}
