﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ppt = Microsoft.Office.Interop.PowerPoint;
using System.ComponentModel;
using System.Windows.Threading;

namespace FanBox.FanBoxLib.Models
{
	public class Slide : INotifyPropertyChanged
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal readonly ppt.Slide pptSlide;
		internal readonly int id;
		internal Sequence sequence;
		internal Shapes shapes;
		internal Dispatcher uiDispatcher;
		#endregion

		#region public properties

		public Shapes Shapes
		{
			get { return this.shapes; }
		}

		#endregion

		#region ctors
		internal Slide(Dispatcher dispatcher, ppt.Slide slide)
		{
			this.uiDispatcher = dispatcher;
			this.pptSlide = slide;
			this.id = slide.SlideID;//SlideID幻灯片的唯一标识符，它具有唯一性
			this.sequence = new Sequence(this ,slide.TimeLine.MainSequence);
			this.shapes = new Shapes(slide.Shapes, this);
		}

		#endregion

		#region public methods
		//TODO 保存与载入
		#endregion

		#region private methods
		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region static methods
		
		#endregion

	}
}
