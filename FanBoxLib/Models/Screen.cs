﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ppt = Microsoft.Office.Interop.PowerPoint;

namespace FanBox.FanBoxLib.Models
{
	/// <summary>
	/// 屏幕画布
	/// </summary>
	public static class Screen
	{
		#region private static data members
		private static double width = 0;
		private static double height = 0;
		private static bool isInitailized = false;
		#endregion

		#region public static properties
		/// <summary>
		/// 尺寸页宽度
		/// </summary>
		static public double Width 
		{
			get 
			{
				if(isInitailized == false)
					System.Windows.MessageBox.Show
						("尺寸信息未初始化成功，请确保运行时事先调用了Screen.Initialize(double,double)方法！");
				return width;
			} 
		}

		/// <summary>
		/// 尺寸页高度
		/// </summary>
		static public double Height
		{
			get
			{
				if (isInitailized == false)
					System.Windows.MessageBox.Show
						("尺寸信息未初始化成功，请确保运行时事先调用了Screen.Initialize(double,double)方法！");
				return height;
			}
		}
		#endregion

		#region ctors
		//NO CTORS
		#endregion

		#region public static methods
		static public void Initialize(double width_, double height_)
		{
			width = width_;
			height = height_;
			isInitailized = true;
		}

		static public double GetProperty(EnumProperty property)
		{
			switch (property)
			{
				case EnumProperty.X:
					return Width;
				case EnumProperty.Y:
					return Height;
				case EnumProperty.Width:
					return Width;
				case EnumProperty.Height:
					return Height;
				case EnumProperty.Opacity:
					return 100d;
				//case ppt.MsoAnimProperty.msoAnimVisibility:
				//	return (int)1;
				default:
					return 1d;
			}
		}

		#endregion

	}
}
