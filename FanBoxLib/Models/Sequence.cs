﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Threading;
using ppt = Microsoft.Office.Interop.PowerPoint;

namespace FanBox.FanBoxLib.Models
{
	/// <summary>
	/// 合成序列对象
	/// </summary>
	internal class Sequence : INotifyPropertyChanged
	{
		#region const
		/// <summary>
		/// 视窗轴点的最小距离：1毫秒
		/// </summary>
		private const double TIME_MIN_DIS = 0.005d;

		/// <summary>
		/// 视窗轴最大预留时长：360000毫秒 => 6分钟
		/// </summary>
		private const int SEQUENCE_MAX_DUR = 360000;

		/// <summary>
		/// 视窗轴最小预留时长：50毫秒
		/// </summary>
		private const int SEQUENCE_MIN_DUR = 50;
		#endregion

		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal Slide parentSlide;//根对象
		internal ppt.Sequence pptSequence;//维护的主序列对象

		internal List<Effect> effectsList;

		private int duration;//合成的预设时间

		#endregion

		#region public properties
		internal Dispatcher UIDispatcher
		{
			get { return this.parentSlide.uiDispatcher; }
		}

		public int Duration
		{
			get { return this.duration; }
			set
			{
				//TODO 超出限制的异常处理
				if (value < SEQUENCE_MIN_DUR || value > SEQUENCE_MAX_DUR)
					return;
				this.duration = value;

				this.PropertyChangedInvoke("Duration");
			}
		}

		#endregion

		#region ctors
		internal Sequence(Slide slide, ppt.Sequence sequence)
		{
			this.parentSlide = slide;
			this.pptSequence = sequence;
			this.effectsList = new List<Effect>();

			this.duration = 0;
		}
		#endregion

		#region public methods

		#endregion

		#region private methods

		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region static methods

	

		#endregion

	}
}
