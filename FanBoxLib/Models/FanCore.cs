﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using ppt = Microsoft.Office.Interop.PowerPoint;
using System.Collections.ObjectModel;

using FanBox.FanBoxLib.Exceptions;

namespace FanBox.FanBoxLib.Models
{
	internal static class FanCoreBugLog
	{
		public static void AddSlide(int id)
		{
			string output = string.Format
				(Resouces.LogMessages.FanCoreAddSlide,id);
			BugLog.Add(output);
		}
	}

	public class FanCore
	{
		#region private data members
		private Dispatcher uiDispatcher;
		private ppt.Application app;
		private List<int> slideIdList;
		private Slide currentSlide;
		#endregion

		#region public properties
		public Dispatcher UIDispatcher
		{
			get { return uiDispatcher; }
		}

		public string FanCanVersion { get=>Resouces.FanCoreInfomation.FanCanVersion; }
		public string FanCanUIVersion { get => Resouces.FanCoreInfomation.FanCanUIVersion; }
		public string FanCoreVersion { get => Resouces.FanCoreInfomation.FanCoreVersion; }
		public string FanBoxVersion { get => Resouces.FanCoreInfomation.FanBoxVersion; }
		public string DevelopSchedule { get => Resouces.FanCoreInfomation.DevelopSchedule; }

		public Slide CurrentSlide
		{
			get
			{
				if (currentSlide == null)
					BugLog.ThrowError(new NullReferenceException());
				return currentSlide;
			}
		}

		#endregion

		public FanCore(Dispatcher dispatcher, ppt.Application application)
		{
			this.uiDispatcher = dispatcher;
			this.app = application;
			this.slideIdList = new List<int>();
			this.currentSlide = null;
		}

		public Slide AddSlide(int id)
		{
			FanCoreBugLog.AddSlide(id);

			ppt.Slides slds = this.app.ActivePresentation.Slides;
			ppt.Slide sld = null;

			if(id <= 0 || id> slds.Count)
				BugLog.ThrowError(new SlideNoIdIsOutOfRangeException(id ,slds.Count));
			
			sld = this.app.ActivePresentation.Slides[id];
			int sldID = sld.SlideNumber;

			if (slideIdList.Contains(sldID))
				BugLog.ThrowError(new FanCoreSlideReloadException(sldID));
			
			slideIdList.Add(sld.SlideID);

			Slide newslide = new Slide(UIDispatcher, sld);
			this.currentSlide = newslide;
			return newslide;
		}

		public void ExportLogs()
		{
			BugLog.Export();
		}

	}
}
