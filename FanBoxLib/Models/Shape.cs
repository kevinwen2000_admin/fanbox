﻿using System;
using System.Collections.Generic;
using ppt = Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Core;
using System.ComponentModel;

namespace FanBox.FanBoxLib.Models
{
	//图形对象
	public class Shape : INotifyPropertyChanged
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region private data members
		internal Shapes parentShapes;//位于Fanbox中的图形集合对象
		internal ppt.Shape pptShape;
		internal Effect mainEffect;
		internal Effect childEffect;
		internal Dictionary<EnumProperty, AnimProperty> propertyDic;
		private string name;
		internal int id;
		private double x, y, width, height, rotation, opacity;
		internal readonly bool isDummy;
		#endregion

		#region public properties
		public string Name
		{
			get { return this.name; }
			set
			{
				this.name = value;
				if (this.isDummy == false)
					this.pptShape.Name = value;
				this.PropertyChangedInvoke("Name");
			}
		}

		#region virtual properties
		public double X
		{
			get
			{
				if (this.isDummy)
					return 0;
				else
					return this.pptShape.Left + this.pptShape.Width / 2;
			}
			set
			{
				if (this.isDummy == false)
					this.pptShape.Left = (float)(value - this.pptShape.Width / 2);
			}
		}

		public double Y
		{
			get
			{
				if (this.isDummy)
					return 0;
				else
					return this.pptShape.Top + this.pptShape.Height / 2;
			}
			set
			{
				if (this.isDummy == false)
					this.pptShape.Top = (float)(value - pptShape.Height / 2);
			}
		}

		public double Width
		{
			get
			{
				if (this.isDummy)
					return 0;
				else
					return pptShape.Width;
			}
			set
			{
				if (this.isDummy == false)
				{
					pptShape.Width = (float)value;
				}
			}
		}

		public double Height
		{
			get
			{
				if (this.isDummy)
					return 0;
				else return pptShape.Height;
			}
			set
			{
				if (this.isDummy == false)
				{
					pptShape.Height = (float)value;

				}
			}
		}
		#endregion

		#region public properties

		/// <summary>
		/// 用于外部显示的接口
		/// </summary>
		public IEnumerable<AnimProperty> Properties
		{
			get => propertyDic.Values;
		}

		#endregion

		#endregion

		#region ctors
		internal Shape(ppt.Shape shape, Shapes shapes, int id, string name)
		{
			this.name = name;
			this.pptShape = shape;
			this.parentShapes = shapes;
			this.propertyDic = new Dictionary<EnumProperty, AnimProperty>();
			this.id = id;
			this.isDummy = (pptShape == null);
			Effect.AddMainEffect(this);
			Effect.AddChildEffect(this);
		}
		#endregion

		#region public methods
		//TODO 现在图形对象的方法为空
		#endregion

		#region priavte methods
		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion

		#region static partition
		private static int emptyShapeCounter;
		private static int CreateEmptyShapeID()
		{
			int rst = -Shape.emptyShapeCounter++;
			return rst;
		}
		static Shape()
		{
			emptyShapeCounter = 0;
		}

		#region temp properties settings
		internal static void PreviewPosition(Shape shape)
		{
			shape.Width = shape.width;
			shape.Height = shape.height;
			shape.X = shape.x;
			shape.Y = shape.y;
		}
		internal static void PresetX(Shape shape, double value)
		{
			shape.x = value;
		}
		internal static void PresetY(Shape shape, double value)
		{
			shape.y = value;
		}
		internal static void PresetWidth(Shape shape, double value)
		{
			shape.width = value;
		}
		internal static void PresetHeight(Shape shape, double value)
		{
			shape.height = value;
		}
		internal static void PresetRotation(Shape shape, double value)
		{
			shape.rotation = value;
		}
		internal static void PresetOpacity(Shape shape, double value)
		{
			shape.opacity = value;
		}

		internal static void AddX(Shape shape, double value)
		{
			shape.x += value;
		}
		internal static void AddY(Shape shape, double value)
		{
			shape.y += value;
		}
		internal static void AddWidth(Shape shape, double value)
		{
			shape.width += value;
		}
		internal static void AddHeight(Shape shape, double value)
		{
			shape.height += value;
		}
		internal static void AddRotation(Shape shape, double value)
		{
			shape.rotation += value;
		}
		internal static void AddOpacity(Shape shape, double value)
		{
			shape.opacity += value;
		}


		#endregion

		internal static void Preset(Shape shape)
		{
			foreach (EnumProperty animProp in PropertyHelper.PropertyTypesArr)
			{
				AnimProperty.Preset(shape.propertyDic[animProp]);
			}
		
		}

		internal static void Preview(Shape shape, int millseconds)
		{
			foreach (EnumProperty animProp in PropertyHelper.PropertyTypesArr)
			{
				AnimProperty.Preview(shape.propertyDic[animProp], millseconds);
			}
		}

		//添加图层
		private static void AddShape(Shapes shapes, ppt.Shape pptshp, int id, string shpName)
		{
			Shape shape = new Shape(null, shapes, id, shpName);

			shapes.UIDispatcher.Invoke(() =>
			{
				shapes.shapeList.Add(shape);
			});

			shapes.shapeDic.Add(id, shape);
		}

		//添加一个空图层
		internal static void AddEmptyShape(Shapes shapes)
		{
			string shpName = "empty";
			int newid = Shape.CreateEmptyShapeID();
			Shape.AddShape(shapes, null, newid, shpName);
		}

		//添加一个指定图层
		internal static void AddNormalShape(Shapes shapes, ppt.Shape pptshp)
		{
			string shpName = pptshp.Name;
			int newid = pptshp.Id;
			Shape.AddShape(shapes, pptshp, newid, shpName);
		}

		#endregion

	}
}
