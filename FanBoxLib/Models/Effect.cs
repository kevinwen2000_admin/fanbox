﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using ppt = Microsoft.Office.Interop.PowerPoint;

namespace FanBox.FanBoxLib.Models
{
	//Effect本质上是一个Behavoir的高级封装，所以在里面会维护一个Behavoir集合
	//其中，每个Effect都是一个图形的效果，在FanBox中，每个图形有且只有唯一的一条动画效果（默认情况）
	//高级的动画管理需要采用空图层
	internal class Effect : INotifyPropertyChanged
	{
		#region const
		private const int MAX_EFFECT_NAME_LENGTH = 20;
		#endregion

		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region private data members
		internal Sequence parentSequence;//父对象
		internal ppt.Effect pptEffect;//效果对象

		private Effect parentEffect;//父级效果
		private List<Effect> childEffectList;//子级效果

		internal ppt.AnimationBehaviors pptBehaviors;//行为对象集合

		internal List<Behavior> animBehaviorList;//行为对象列表
		internal List<Behavior> propBehaviorList;//行为对象列表
												 //用于索引查询的字典，与行为对象列表一起使用
		[Obsolete("因为我们对对象的结构进行了重构，所有的属性都会被放到AnimProperty中，此处对象将无任何意义", true)]
		internal readonly Dictionary<string, Behavior> behaviorsDic;
		[Obsolete("因为我们对对象的结构进行了重构，所有的属性都会被放到AnimProperty中，此处对象将无任何意义", true)]
		internal readonly Dictionary<EnumProperty, Behavior> propertyDic;

		internal readonly Shape shape;//绑定图形

		//TODO Effect的时间重构
		internal int duration;//效果时长
		internal int delayTime;//延迟

		#endregion

		#region public properties
		internal Dispatcher UIDispatcher
		{
			get { return this.parentSequence.UIDispatcher; }
		}

		public bool IsEmptyEffect
		{
			get => this.shape.isDummy;
		}

		public bool IsIndividual
		{
			get { return this.parentEffect == null; }
		}

		#endregion

		#region ctors
		internal Effect(Sequence pSequence) : this(null, null, pSequence)
		{
		}

		internal Effect(Shape hostShape, ppt.Effect effect, Sequence pSequence)
		{
			this.parentSequence = pSequence;
			this.shape = hostShape;

			this.pptEffect = effect;

			this.parentEffect = null;
			this.childEffectList = new List<Effect>();

			this.pptBehaviors = effect?.Behaviors;
			this.animBehaviorList = new List<Behavior>();
			this.propBehaviorList = new List<Behavior>();

			this.duration = 30000;
			this.delayTime = 0;
		}
		#endregion

		#region public methods

		public void Delete()
		{
			//效果的删除，此处需要
		}

		public void Preview(int millseconds)
		{
			foreach (Behavior propbhv in this.propBehaviorList)
			{
				propbhv.Preset();
			}

			foreach (Behavior animbhv in this.animBehaviorList)
			{
				animbhv.Preview(millseconds);
			}
		}

		public override string ToString()
		{
			string head = $"[Effect]";
			string durStr = $"duration:{this.duration}";
			string delayStr = $"delay:{this.delayTime}";
			string bhvsStr = $"behavior_count:{this.animBehaviorList.Count}";
			return $"{head}\t{durStr}\t{delayStr}\t{bhvsStr}";
		}

		#endregion

		#region private methods

		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region private properties

		#endregion

		#region static methods
		internal static bool IsInDurationRange(Effect effect, int millseconds)
		{
			millseconds.MillSecondslize();
			int begintime = effect.delayTime;
			int totaltime = effect.delayTime + effect.duration;
			return millseconds > begintime && millseconds < totaltime;
		}


		//向Sequence申请一个一条新的效果，不在Shape中进行注册
		private static Effect CreateEmptyEffect(Shape hostShape)
		{
			Sequence sequence = hostShape.parentShapes.parentSlide.sequence;
			ppt.Shape shp = hostShape.pptShape;
			ppt.Sequence seqc = sequence.pptSequence;
			ppt.Effect eff = null;

			if (hostShape.isDummy == false)
			{
				eff = seqc?.AddEffect(
				Shape: shp,
				effectId: ppt.MsoAnimEffect.msoAnimEffectCustom,
				trigger: ppt.MsoAnimTriggerType.msoAnimTriggerWithPrevious);
			}

			Effect newEffect = new Effect(hostShape, eff, sequence);
			sequence.effectsList.Add(newEffect);
			return newEffect;
		}

		internal static Effect AddMainEffect(Shape hostShape)
		{

			Effect newEffect = Effect.CreateEmptyEffect(hostShape);

			//静态属性动画
			Behavior propx = Behavior.AddPropertyBehavior(newEffect, EnumProperty.X);
			Behavior propy = Behavior.AddPropertyBehavior(newEffect, EnumProperty.Y);
			Behavior propw = Behavior.AddPropertyBehavior(newEffect, EnumProperty.Width);
			Behavior proph = Behavior.AddPropertyBehavior(newEffect, EnumProperty.Height);
			Behavior propr = Behavior.AddPropertyBehavior(newEffect, EnumProperty.Rotation);
			Behavior propo = Behavior.AddPropertyBehavior(newEffect, EnumProperty.Opacity);

			//动画序列轴
			Behavior animx = Behavior.AddStandardBehavior(newEffect, EnumProperty.X);
			Behavior animy = Behavior.AddStandardBehavior(newEffect, EnumProperty.Y);
			Behavior animw = Behavior.AddStandardBehavior(newEffect, EnumProperty.Width);
			Behavior animh = Behavior.AddStandardBehavior(newEffect, EnumProperty.Height);
			Behavior animr = Behavior.AddStandardBehavior(newEffect, EnumProperty.Rotation);
			Behavior animo = Behavior.AddStandardBehavior(newEffect, EnumProperty.Opacity);

			//静态属性轴注册

			propx.Value = hostShape.X;
			propy.Value = hostShape.Y;
			propw.Value = hostShape.Width;
			proph.Value = hostShape.Height;

			propo.headPoint.Value = "100";
			propo.rearPoint.Value = "100";

			AnimProperty.AddAnimProperty(hostShape, EnumProperty.X, propx, animx);
			AnimProperty.AddAnimProperty(hostShape, EnumProperty.Y, propy, animy);
			AnimProperty.AddAnimProperty(hostShape, EnumProperty.Width, propw, animw);
			AnimProperty.AddAnimProperty(hostShape, EnumProperty.Height, proph, animh);
			AnimProperty.AddAnimProperty(hostShape, EnumProperty.Rotation, propr, animr);
			AnimProperty.AddAnimProperty(hostShape, EnumProperty.Opacity, propo, animo);

			hostShape.mainEffect = newEffect;

			return newEffect;
		}

		//创建子效果
		internal static Effect AddChildEffect(Shape hostShape)
		{
			Effect newEffect = Effect.CreateEmptyEffect(hostShape);
			hostShape.childEffect = newEffect;
			return newEffect;
		}

		internal static int GetDuration(Effect effect)
		{
			if (effect.IsIndividual)
				return effect.duration;
			else
				return Effect.GetDuration(effect.parentEffect);
		}

		internal static void SetDuration(Effect effect, int millseconds)
		{
			if (effect.IsIndividual == false)
				return;
			int ms = millseconds.ToMillSeconds();
			float snd = millseconds.ToSeconds();

			effect.duration = ms;
			if (effect.IsEmptyEffect)
				effect.pptEffect.Timing.Duration = snd;

			foreach (Effect childEffect in effect.childEffectList)
			{
				Effect.SetDurationBinded(childEffect, snd);
			}
		}

		//因为里面的参数都是用于显示的，而绑定对象的所有都是与父对象一致的存在
		//因此，我们可以节省在这边的一些赋值与传参的开销。
		//虽然这没有什么太大的意义。
		private static void SetDurationBinded(Effect effect, float seconds)
		{
			//effect.duration = millseconds;
			if (effect.IsEmptyEffect)
				effect.pptEffect.Timing.Duration = seconds;
			foreach (Effect childEffect in effect.childEffectList)
			{
				Effect.SetDurationBinded(childEffect, seconds);
			}
		}

		internal static int GetDelayTime(Effect effect)
		{
			if (effect.IsIndividual)
				return effect.delayTime;
			else
				return Effect.GetDelayTime(effect.parentEffect);
		}

		internal static void SetDelayTime(Effect effect, int millseconds)
		{
			if (effect.IsIndividual == false)
				return;
			int ms = millseconds.ToMillSeconds();
			float snd = millseconds.ToSeconds();

			effect.delayTime = ms;
			if (effect.IsEmptyEffect)
				effect.pptEffect.Timing.TriggerDelayTime = snd;

			foreach (Effect childEffect in effect.childEffectList)
			{
				Effect.SetDelayTimeBinded(childEffect, snd);
			}
		}

		//我们认为减少一个方法的运算是一件很有趣的事情，通过直接赋值，可以解决运算不一致之类的问题。
		//里面的参数都是从SetDelayTime传递下来的，在SetDelayTime这个入口里面已经检查过数字的正确性了
		//所以不需要对数字进行第二步的修正
		private static void SetDelayTimeBinded(Effect effect, float seconds)
		{
			//effect.delayTime = millseconds;
			if (effect.IsEmptyEffect)
				effect.pptEffect.Timing.TriggerDelayTime = seconds;
			foreach (Effect childEffect in effect.childEffectList)
			{
				Effect.SetDelayTimeBinded(childEffect, seconds);
			}
		}
		#endregion
	}
}
