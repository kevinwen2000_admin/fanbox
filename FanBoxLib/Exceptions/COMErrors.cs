﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{
	public class PowerPointMissingException : Exception
	{
		public PowerPointMissingException() { }
		public PowerPointMissingException(string message) : base(message) { }
		public PowerPointMissingException(string message, Exception inner) : base(message, inner) { }
		protected PowerPointMissingException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}
