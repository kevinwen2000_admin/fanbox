﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{
	[Serializable]
	public class ShapesException : Exception
	{
		public ShapesException() { }
		public ShapesException(string message) : base(message) { }
		public ShapesException(string message, Exception inner) : base(message, inner) { }
		protected ShapesException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

	#region 至多注册一组动画

	[Serializable]
	public class ShapesCollapseEffectException : Exception
	{
		public ShapesCollapseEffectException() :
			base(Resouces.ExceptionMessages.ShapesCannotAddAnthorEffectAnymoreErrorMsg) { }
	}
	#endregion

}
