﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FanBox.FanBoxLib.Exceptions
{
	public static class BugLog
	{
		static List<string> logs;
		static BugLog()
		{
			logs = new List<string>();
		}

		public static void Add(string loginfo)
		{
			logs.Add($"{DateTime.Now} {loginfo}\n");
		}

		public static void ThrowError(string error)
		{
			BugLog.ThrowError(new Exception(error));
		}

		public static void ThrowError(Exception ex)
		{
			Add($"[Error] {ex.Message}");
			throw ex;
		}

		public static void Export()
		{
			string dir = $@"{Environment.CurrentDirectory}\logs\";
			DateTime now = DateTime.Now;
			string path = $@"{dir}{now.ToLongDateString()}{now.ToLongTimeString().Replace(':','-')}.log";
			//DirectoryInfo outputdir = null;
			if (Directory.Exists(dir))
			{
				//outputdir = new DirectoryInfo(dir);
			}
			else
			{
				Directory.CreateDirectory(dir);
			}

			FileStream fileStream = new FileStream(path, FileMode.Create);
			foreach (string item in logs)
			{
				byte[] bytes = Encoding.UTF8.GetBytes(item);
				fileStream.Write(bytes, 0, bytes.Length);
			}
			fileStream.Close();

			Process p = new Process();
			p.StartInfo.FileName = "explorer.exe";
			p.StartInfo.Arguments = dir;
			p.Start();

		}

	}


}
