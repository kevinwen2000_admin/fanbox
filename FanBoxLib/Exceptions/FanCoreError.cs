﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{

	[Serializable]
	public class FanCoreException : Exception
	{
		public FanCoreException() { }
		public FanCoreException(string message) : base(message) { }
		public FanCoreException(string message, Exception inner) : base(message, inner) { }
		protected FanCoreException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) 
		{
		}
	}

	#region 幻灯片重复加载
	[Serializable]
	public class FanCoreSlideReloadException : FanCoreException
	{
		public FanCoreSlideReloadException(int sldid) :
			base(string.Format(Resouces.ExceptionMessages.FanCoreSlideIdReloadErrorMsg, sldid))
		{ }
	}
	#endregion

	#region 幻灯片超出范围
	[Serializable]
	public class SlideNoIdIsOutOfRangeException : FanCoreException
	{
		public SlideNoIdIsOutOfRangeException(int targetSldid, int maxSldid) :
			base(string.Format(Resouces.ExceptionMessages.FanCoreSlideIdOutOfRangeErrorMsg, targetSldid, maxSldid))
		{ }
	}
	#endregion

}
