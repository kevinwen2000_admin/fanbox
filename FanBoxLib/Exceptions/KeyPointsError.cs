﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{

	[Serializable]
	public class KeyPointsException : Exception
	{
		public KeyPointsException() { }
		public KeyPointsException(string message) : base(message) { }
		public KeyPointsException(string message, Exception inner) : base(message, inner) { }
		protected KeyPointsException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

	#region 帧时重复
	public class KeyPointsKeyPointReloadException : KeyPointsException
	{
		public KeyPointsKeyPointReloadException(int millseconds) :
			base(string.Format(Resouces.ExceptionMessages.KeyPointsKeyPointReloadErrorMsg, millseconds))
		{ }
	}
	#endregion

	#region 帧时超出范围

	[Serializable]
	public class KeyPointsNewTimeOutOfRangeException : KeyPointsException
	{
		public KeyPointsNewTimeOutOfRangeException() :
			base(Resouces.ExceptionMessages.KeyPointsNewTimeOutOfRangeErrorMsg)
		{ }

	}
	#endregion

}
