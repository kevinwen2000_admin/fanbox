﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{

	[Serializable]
	public class EffectException : Exception
	{
		public EffectException() { }
		public EffectException(string message) : base(message) { }
		public EffectException(string message, Exception inner) : base(message, inner) { }
		protected EffectException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}


}
