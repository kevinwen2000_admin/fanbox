﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{

	[Serializable]
	public class BehaviorException : Exception
	{
		public BehaviorException() { }
		public BehaviorException(string message) : base(message) { }
		public BehaviorException(string message, Exception inner) : base(message, inner) { }
		protected BehaviorException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

	#region 复制问题
	[Serializable]
	public class BehaviorBindBoneException : BehaviorException
	{
		public BehaviorBindBoneException() : 
			base(Resouces.ExceptionMessages.BehaviorBindSelfErrorMsg) { }
	}
	#endregion




}
