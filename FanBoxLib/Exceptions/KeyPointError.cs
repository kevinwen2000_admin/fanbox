﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBox.FanBoxLib.Exceptions
{
	[Serializable]
	public class KeyPointException : Exception
	{
		public KeyPointException() { }
		public KeyPointException(string message) : base(message) { }
		public KeyPointException(string message, Exception inner) : base(message, inner) { }
		protected KeyPointException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

	#region 帧时超出范围

	[Serializable]
	public class KeyPointTimeOutOfRangeException : KeyPointException
	{
		public KeyPointTimeOutOfRangeException() :
			base(Resouces.ExceptionMessages.KeyPointTimeOutOfRangeErrorMsg)
		{ }
	}


	#endregion

	#region 无法修改锁定对象

	[Serializable]
	public class KeyPointLockProtectedException : KeyPointException
	{
		public KeyPointLockProtectedException() :
			base(Resouces.ExceptionMessages.KeyPointLockProtectedErrorMsg)
		{ }
	}
	#endregion

	#region 无法修改非独立对象

	[Serializable]
	public class KeyPointIndividualProtectedException : KeyPointException
	{
		public KeyPointIndividualProtectedException() :
			base(Resouces.ExceptionMessages.KeyPointIndividualProtectedErrorMsg)
		{ }
	}
	#endregion

	#region 无法修改补间对象

	[Serializable]
	public class KeyPointTweenProtectedException : KeyPointException
	{
		public KeyPointTweenProtectedException() :
			base(Resouces.ExceptionMessages.KeyPointIsTweenErrorMsg)
		{ }
	}
	#endregion



}
