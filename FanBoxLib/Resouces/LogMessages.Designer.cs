﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace FanBox.FanBoxLib.Resouces {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class LogMessages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LogMessages() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FanBox.FanBoxLib.Resouces.LogMessages", typeof(LogMessages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   重写当前线程的 CurrentUICulture 属性，对
        ///   使用此强类型资源类的所有资源查找执行重写。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}主绑定图形{2}属性。 的本地化字符串。
        /// </summary>
        internal static string BehaviorBindBone {
            get {
                return ResourceManager.GetString("BehaviorBindBone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 在FanCore中添加[{0}]幻灯片。 的本地化字符串。
        /// </summary>
        internal static string FanCoreAddSlide {
            get {
                return ResourceManager.GetString("FanCoreAddSlide", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}，添加帧时{2}，帧值{3}，帧表达式{4}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointAddKeyPoint {
            get {
                return ResourceManager.GetString("KeyPointAddKeyPoint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，调整帧时{3}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointAdjustTime {
            get {
                return ResourceManager.GetString("KeyPointAdjustTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，拷贝帧时{3}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointCopy {
            get {
                return ResourceManager.GetString("KeyPointCopy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，拷贝到图形{3}属性{4}帧时{5}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointCopyTo {
            get {
                return ResourceManager.GetString("KeyPointCopyTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，删除。 的本地化字符串。
        /// </summary>
        internal static string KeyPointDelete {
            get {
                return ResourceManager.GetString("KeyPointDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，移动帧时{3}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointMoveTime {
            get {
                return ResourceManager.GetString("KeyPointMoveTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}，添加帧时{2}，帧值{3}，帧表达式{4}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointsAddKeyPoint {
            get {
                return ResourceManager.GetString("KeyPointsAddKeyPoint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}清除所有帧点。 的本地化字符串。
        /// </summary>
        internal static string KeyPointsClearAll {
            get {
                return ResourceManager.GetString("KeyPointsClearAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，删除。 的本地化字符串。
        /// </summary>
        internal static string KeyPointsDeletePointKey {
            get {
                return ResourceManager.GetString("KeyPointsDeletePointKey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，修改帧表达式{3}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointVisitFormula {
            get {
                return ResourceManager.GetString("KeyPointVisitFormula", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，修改帧时{3}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointVisitTime {
            get {
                return ResourceManager.GetString("KeyPointVisitTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 图形{0}属性{1}位于{2}秒关键帧，修改帧值{3}。 的本地化字符串。
        /// </summary>
        internal static string KeyPointVisitValue {
            get {
                return ResourceManager.GetString("KeyPointVisitValue", resourceCulture);
            }
        }
    }
}
