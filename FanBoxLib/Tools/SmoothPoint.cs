﻿using System.ComponentModel;

namespace FanBox.FanBoxLib.Tools
{
	public class SmoothPoint:INotifyPropertyChanged,INotifyPropertyChanging
	{
		#region public events handler
		public event PropertyChangedEventHandler PropertyChanged;
		public event PropertyChangingEventHandler PropertyChanging;
		#endregion

		#region private data members
		private double x, y;
		#endregion

		#region public properties
		public double X
		{
			get { return this.x; }
			set
			{
				double rst = value;
				if (value < 0d)
					rst = 0d;
				else if (value > 1d)
					rst = 1d;

				this.x = rst;
				this.PropertyChangedInvoke("X");
			}
		}

		public double Y
		{
			get { return this.y; }
			set
			{
				double rst = value;
				if (value < 0d)
					rst = 0d;
				else if (value > 1d)
					rst = 1d;

				this.y = rst;
				this.PropertyChangedInvoke("Y");
			}
		}
		#endregion

		#region ctors
		internal SmoothPoint()
		{
			this.x = 0d;
			this.y = 0d;
		}

		internal SmoothPoint(SmoothPoint smoothPoint)
		{
			this.x = smoothPoint.x;
			this.y = smoothPoint.y;
		}

		#endregion

		#region public methods
		//公共方法
		#endregion

		#region private methods
		private void PropertyChangedInvoke(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private void PropertyChangingInvoke(string propertyName)
		{
			PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
		}
		#endregion

		#region static methods
		//静态方法
		#endregion

	}
}
