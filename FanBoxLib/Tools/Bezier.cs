﻿using System;
namespace FanBox.FanBoxLib.Tools
{
	//贝塞尔曲线
	public static class Bezier
	{
		//三阶贝塞尔
		public static double ThreeCube(double t,double peerA, double adjustA, double adjustB, double peerB)
		{
			int cube = 3;
			double rtn = 0d;
			double rt = 1 - t;
			double[] paramArr = { peerA, adjustA, adjustB, peerB };
			for (int n = 0; n < 4; n++)
			{
				int m = cube - n;
				rtn += Combinate(3, n) * Math.Pow(t, n) * Math.Pow(rt, m) * paramArr[n];
			}
			return rtn;
		}

		//组合数的求取
		private static int Combinate(int n, int m)
		{
			int a = 1;
			int b = 1;
			for (int i = 1; i <= m; i++)
			{
				a *= n - i + 1;
				b *= i;
			}

			return a/b;
		}

	}
}
