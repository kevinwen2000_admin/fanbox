﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FanBox.FanBoxLib.Tools
{
	public enum EnumEleType
	{
		Null, Constant, Number, Operator, Function, Completed
	}

	public struct Ele
	{
		public readonly string Value;
		public readonly EnumEleType EleType;
		public Ele(char ele, EnumEleType eleType)
		{
			Value = ele.ToString();
			EleType = eleType;
		}
		public Ele(string ele, EnumEleType eleType)
		{
			Value = ele;
			EleType = eleType;
		}

		public override string ToString()
		{
			return this.Value;
		}

		public static Ele Empty = new Ele("null", EnumEleType.Null);


	}

	public class CustomFunction
	{
		public readonly string Name;
		public readonly string Expression;
		public delegate double Calculate(double num);
		private Calculate calculate = null;

		public CustomFunction(string name, string expression, Calculate calculate)
		{
			this.Name = name;
			this.Expression = expression;
			this.calculate = calculate;
		}

		public string Parse(Ele ele)
		{
			return this.Parse(ele.Value);
		}

		public string Parse(string vari)
		{
			return string.Format(Expression, $"({vari})");
		}

		public double Invoke(double num)
		{
			return this.calculate(num);
		}
	}

	public static class FormulaAnalyzer
	{
		#region static preset
		private static HashSet<char> UnaryOp;
		private static HashSet<char> LowBinaryOp;
		private static HashSet<char> HeighBinaryOp;
		private static HashSet<char> TopBinaryOp;
		private static HashSet<string> FunctionSet;
		public readonly static Dictionary<string, CustomFunction> CustomDic;
		static FormulaAnalyzer()
		{
			UnaryOp = new HashSet<char>();
			LowBinaryOp = new HashSet<char>();
			HeighBinaryOp = new HashSet<char>();
			TopBinaryOp = new HashSet<char>();
			FunctionSet = new HashSet<string>();
			CustomDic = new Dictionary<string, CustomFunction>();

			UnaryOp.Add('-');
			LowBinaryOp.Add('-');
			LowBinaryOp.Add('+');
			HeighBinaryOp.Add('*');
			HeighBinaryOp.Add('/');
			HeighBinaryOp.Add('%');
			TopBinaryOp.Add('^');
			FunctionSet.Add("empty");
			FunctionSet.Add("sin");
			FunctionSet.Add("cos");
			FunctionSet.Add("tan");
			FunctionSet.Add("rand");
			FunctionSet.Add("ceil");
			FunctionSet.Add("floor");
			FunctionSet.Add("sqrt");

			CustomDic.Add("test", new CustomFunction("test", "{0}+{0}+{0}", (a) => 3 * a));

			CustomDic.Add("flash", new CustomFunction("flash", "(0.5-rand(1))", (a) => 0.5 * a));
			CustomDic.Add("sinw", new CustomFunction("sinw", "(sin((2*pi)*{0}))", (a) =>
			{
				return Math.Sin(2 * Math.PI * a);
			}));
			CustomDic.Add("cosw", new CustomFunction("cosw", "(cos((2*pi)*{0}))", (a) =>
			{
				return Math.Cos(2 * Math.PI * a);
			}));
			CustomDic.Add("tooth", new CustomFunction("tooth", "($-floor($))", (a) =>
			{
				return a - Math.Floor(a);
			}));
			CustomDic.Add("rect", new CustomFunction("rect", "((-1)^floor(2*$)", (a) =>
			{
				if (Math.Floor(a) % 2 == 0)
					return -1;
				else
					return +1;
			}));

			{
				string ffuncfmt = "((0-1)^floor(2*({0}))*1+1)";//0,0.5=> 1, 0
				string gpfuncfmt = "(({0})-floor(({0})))";//0~0.5 =>0~1
				string gmfuncfmt = "(0.5-(({0})-floor(({0}))))";//0~0.5 =>0~0.5
				string itemf1 = string.Format(ffuncfmt, "$+0.25");
				string itemf2 = string.Format(ffuncfmt, "$-0.25");
				string itemg1 = string.Format(gpfuncfmt, "$+0.25");
				string itemg2 = string.Format(gmfuncfmt, "$-0.25");
				string rst = $"(2*({itemf1}*{itemg1}+{itemf2}*{itemg2})-1)";

				CustomDic.Add("tri", new CustomFunction("tri", rst, (a) =>
				{
					double d = a - Math.Floor(a);
					if (d <= 0.25)
						return 4 * d;
					else if (0.25 <= d && d <= 0.75)
						return 4 * (0.5 - d);
					else if (d >= 0.75)
						return 4 * (d - 1);
					return 0d;
				}));
			}

		}
		#endregion

		#region check state
		private static bool IsMeaningLessChar(char c)
		{
			return c == ' ' || c == '\n' || c == '\r';
		}

		private static bool IsValidFunctionName(ref string funName)
		{
			return FunctionSet.Contains(funName) || CustomDic.Keys.Contains(funName);
		}

		private static bool IsCustomFunction(Ele ele)
		{
			return CustomDic.Keys.Contains(ele.Value);
		}

		private static bool IsDigit(char c)
		{
			if (c == '.' || (c >= '0' && c <= '9') || c == '$')
				return true;
			return false;
		}

		private static bool IsLowOperator(char c)
		{
			return LowBinaryOp.Contains(c);
		}

		private static bool IsHeightOperator(char c)
		{
			return HeighBinaryOp.Contains(c);
		}

		private static bool IsTopOperator(char c)
		{
			return TopBinaryOp.Contains(c);
		}

		//判断m与c的操作符优先级，如果m优先级要低于c，则返回true
		//如果操作符优先度较低，则可以对前方优先级高的式子进行计算
		private static bool CompareLowerRank(Ele m, Ele c)
		{
			return CompareLowerRank(m.Value[0], c.Value[0]);
		}

		private static bool CompareLowerRank(char m, char c)
		{
			if (IsLowOperator(m))
				return true;
			else if (IsTopOperator(m))
				return false;
			else
			{
				if (IsLowOperator(c) == false)
					return true;
				else
					return false;
			}
		}

		private static bool IsOperator(char c)
		{
			return HeighBinaryOp.Contains(c) || LowBinaryOp.Contains(c) || TopBinaryOp.Contains(c);
		}

		private static bool IsFuncChar(char c)
		{
			return char.IsLetter(c);
		}

		//是否为负数
		private static bool IsMinus(string numstr)
		{
			foreach (char num in numstr)
			{
				if (num == '-') return true;
			}
			return false;
		}

		private static bool IsValidNumber(ref string numstr)
		{
			int len = numstr.Length;
			int i = len - 1;
			int dot = 0;
			int key = 0;
			char cchar = numstr[i];

			if (cchar == '.') return false;

			while (i > -1)
			{
				cchar = numstr[i];
				if (cchar == '.') dot++;
				else if (cchar == '$') key++;
				i--;
			}

			if (key != 0 && len != 1) return false;
			if (dot > 1) return false;

			return true;
		}

		private static double EleParse(Ele ele, double? vari = null)
		{
			if (vari != null)
			{
				if (ele.Value == "$")
					return (double)vari;
				else if (ele.Value == "-$")
					return (double)-vari;
			}


			if (ele.Value == "e")
				return Math.E;
			else if (ele.Value == "pi")
				return Math.PI;
			else
				return double.Parse(ele.Value);
		}

		private static EnumEleType GetLastEleType(ref List<Ele> list)
		{
			if (list.Count == 0) return EnumEleType.Null;
			return list.Last().EleType;
		}

		//是否将减号视为负号
		private static bool AsMinus(ref string formula, int start, EnumEleType prevType)
		{
			int len = formula.Length;
			char cchar;
			int np = start + 1;

			if (prevType == EnumEleType.Number ||
				prevType == EnumEleType.Constant ||
				prevType == EnumEleType.Completed)
				return false;
			else if
				(prevType == EnumEleType.Null ||
				prevType == EnumEleType.Function)
				return true;

			while (np < len)
			{
				cchar = formula[np];
				if (IsDigit(cchar) == false)
					break;
				np++;
			}
			if (np >= len) return false;
			cchar = formula[np];
			if (IsLowOperator(cchar))
				return true;
			else if (IsHeightOperator(cchar) || IsTopOperator(cchar))
				return false;
			else if (cchar == ')')
				return true;
			return false;
		}

		#endregion

		#region extract elements
		//用于计算的元素，用于语义拆分


		//提取数字
		private static string ExtractConstant(ref string formula, int start, out int subpos, EnumEleType prevType)
		{
			if (prevType == EnumEleType.Completed ||
				prevType == EnumEleType.Number)
				throw new Exception($"#{start - 1}位置需要运算符");

			subpos = 0;
			char cchar = formula[start + subpos];
			string rst = "";
			int len = formula.Length;




			if (cchar == 'e')
			{
				subpos = 1;
				rst = "e";
			}

			else if (start + 1 < len && cchar == 'p')
			{
				char nchar = formula[start + 1];
				if (nchar == 'i')
				{
					subpos = 2;
					rst = "pi";
				}
			}

			if (subpos == 0) return "";
			else if (start + subpos >= len) return rst;

			char mchar = formula[start + subpos + 1];
			if (IsOperator(mchar) || IsMeaningLessChar(mchar))
				return rst;
			else
				return "";
		}
		private static string ExtractNumber(ref string formula, int start, out int subpos, EnumEleType prevType)
		{
			if (prevType == EnumEleType.Completed ||
				prevType == EnumEleType.Constant)
				throw new Exception($"#{start}位置需要运算符");
			subpos = 0;
			int len = formula.Length;
			while (true)
			{
				int p = start + subpos;
				if (p >= len) break;
				char cchar = formula[p];

				if (IsDigit(cchar))
					subpos++;
				else
					break;
			}
			string rst = formula.Substring(start, subpos);
			if (IsValidNumber(ref rst))
				return rst;
			else
				throw new Exception($"#{start}位置数字{rst}不合法");
		}

		private static string ExtractOperator(ref string formula, int start, out int subpos, EnumEleType prevType)
		{
			if (prevType == EnumEleType.Operator ||
				prevType == EnumEleType.Null)
				throw new Exception($"#{start}位置运算符操作数不完整");
			subpos = 1;

			return formula.Substring(start, subpos);
		}

		private static string ExtractFunctionName(ref string formula, int start, out int subpos, EnumEleType prevType)
		{
			if (prevType == EnumEleType.Completed ||
				prevType == EnumEleType.Number ||
				prevType == EnumEleType.Constant)
				throw new Exception($"#{start - 1}位置函数名不合法");

			subpos = 1;
			int front = 0;

			while (true)
			{
				int p = start - front;
				if (p < 0) break;
				char cchar = formula[p];

				if (IsFuncChar(cchar))
					front++;
				else
					break;
			}
			string rst = formula.Substring(start - front + 1, front);
			if (front == 0) rst = "empty";
			if (IsValidFunctionName(ref rst))
				return rst;
			else
				throw new Exception($"#{start}位置函数名{rst}未找到");
		}

		private static string ExtractSpecialMark(ref string formula, int start, out int subpos, EnumEleType prevType)
		{
			if (prevType == EnumEleType.Operator ||
				prevType == EnumEleType.Null)
				throw new Exception($"#{start - 1}位置缺少数字");
			subpos = 1;
			return "completed";
		}

		private static void ExtractOtherCase(ref string formula, int start, out int subpos, EnumEleType prevType)
		{
			char cchar = formula[start];
			subpos = 1;
			if (IsMeaningLessChar(cchar) || char.IsLetter(cchar))
				return;
			else
				throw new Exception($"#{start}无法解析{cchar}字符");
		}

		private static double CalculateOperator(ref Ele operatorEle, double numEle1, double numEle2)
		{
			char cchar = operatorEle.Value[0];
			double num1 = numEle1;
			double num2 = numEle2;
			if (cchar == '+')
				return num1 + num2;
			else if (cchar == '-')
				return num1 - num2;
			else if (cchar == '*')
				return num1 * num2;
			else if (cchar == '/')
				return num1 / num2;
			else if (cchar == '%')
				return num1 % num2;
			else if (cchar == '^')
				return Math.Pow(num1, num2);
			throw new Exception("未知运算符");
		}

		private static double CalculateFunction(ref Ele functionEle, double numEle)
		{
			string eleValue = functionEle.Value;
			double num = numEle;
			if (eleValue == "empty")
				return num;
			else if (eleValue == "sin")
				return Math.Sin(num);
			else if (eleValue == "cos")
				return Math.Cos(num);
			else if (eleValue == "tan")
				return Math.Tan(num);
			else if (eleValue == "rand")
				return 0.5 * num;
			else if (eleValue == "ceil")
				return Math.Ceiling(num);
			else if (eleValue == "floor")
				return Math.Floor(num);
			else if (eleValue == "sqrt")
				return Math.Sqrt(num);
			else
				throw new Exception("未知函数名");
		}

		#endregion

		#region core

		//语素拆解
		public static Ele[] Splite(string formula)
		{
			List<Ele> eles = new List<Ele>();
			int head = 0;
			int len = formula.Length;
			EnumEleType lastType;
			int matchCounter = 0;
			eles.Add(new Ele("empty", EnumEleType.Function));
			while (head < len)
			{
				lastType = GetLastEleType(ref eles);
				int subpos = 0;
				char cchar = formula[head];
				string rst;

				if (IsDigit(cchar))
				{
					rst = ExtractNumber(ref formula, head, out subpos, lastType);

					eles.Add(new Ele(rst, EnumEleType.Number));

				}
				else if (cchar == '-')
				{
					if (AsMinus(ref formula, head, lastType))
					{
						head = head + 1;
						rst = ExtractNumber(ref formula, head, out subpos, lastType);
						eles.Add(new Ele($"-{rst}", EnumEleType.Number));
					}
					else
					{
						rst = ExtractOperator(ref formula, head, out subpos, lastType);
						eles.Add(new Ele(rst, EnumEleType.Operator));
					}
				}
				else if (IsOperator(cchar))
				{
					rst = ExtractOperator(ref formula, head, out subpos, lastType);
					eles.Add(new Ele(rst, EnumEleType.Operator));
				}
				else if (cchar == 'e' || cchar == 'p')
				{
					rst = ExtractConstant(ref formula, head, out subpos, lastType);
					if (rst.Length == 0)
						subpos = 1;
					else
						eles.Add(new Ele(rst, EnumEleType.Constant));
				}
				else if (cchar == '(')
				{
					rst = ExtractFunctionName(ref formula, head - 1, out subpos, lastType);
					eles.Add(new Ele(rst, EnumEleType.Function));
					matchCounter++;
				}
				else if (cchar == ')')
				{
					rst = ExtractSpecialMark(ref formula, head, out subpos, lastType);
					eles.Add(new Ele(rst, EnumEleType.Completed));
					matchCounter--;
				}
				else
				{
					ExtractOtherCase(ref formula, head, out subpos, lastType);
				}

				if (matchCounter < 0)
					throw new Exception($"括号不匹配");

				head += subpos;
			}
			eles.Add(new Ele("completed", EnumEleType.Completed));
			if (matchCounter != 0)
				throw new Exception("括号不匹配");
			return eles.ToArray();
		}

		public static double Calculate(Ele[] eles, double var)
		{
			Stack<double> valueStack = new Stack<double>();
			Stack<Ele> operationStack = new Stack<Ele>();

			int i = 0;
			int len = eles.Length;
			while (i < len)
			{
				Ele ele = eles[i];
				//入栈操作((-1)^floor(2*(0))*0.5+0.5)
				switch (ele.EleType)
				{
					case EnumEleType.Null:
						i++;
						break;
					case EnumEleType.Constant:
						valueStack.Push(EleParse(ele));
						//放入时
						if (valueStack.Count > 1)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Operator)
							{
								double n2 = valueStack.Pop();
								double n1 = valueStack.Pop();
								operationStack.Pop();
								double rst = CalculateOperator(ref topEle, n1, n2);
								valueStack.Push(rst);
							}
						}
						i++;
						break;
					case EnumEleType.Number:
						valueStack.Push(EleParse(ele, var));
						if (valueStack.Count > 1)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Operator)
							{
								double n2 = valueStack.Pop();
								double n1 = valueStack.Pop();
								operationStack.Pop();
								double rst = CalculateOperator(ref topEle, n1, n2);
								valueStack.Push(rst);
							}
						}
						i++;
						break;
					case EnumEleType.Operator:
						if (valueStack.Count > 1 && operationStack.Count > 1)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Operator && CompareLowerRank(ele, topEle))
							{
								double n2 = valueStack.Pop();
								double n1 = valueStack.Pop();
								operationStack.Pop();
								double rst = CalculateOperator(ref topEle, n1, n2);
								valueStack.Push(rst);
							}
							else
							{
								operationStack.Push(ele);
								i++;
							}
						}
						else
						{
							operationStack.Push(ele);
							i++;
						}
						break;
					case EnumEleType.Function:
						operationStack.Push(ele);
						i++;
						break;
					case EnumEleType.Completed:
						if (valueStack.Count > 0)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Function)
							{
								double n = valueStack.Pop();
								operationStack.Pop();
								double rst = 0d;
								if (IsCustomFunction(topEle))
									rst = CustomDic[topEle.Value].Invoke(n);
								else
									rst = CalculateFunction(ref topEle, n);
								valueStack.Push(rst);
								i++;
							}
							else if (topEle.EleType == EnumEleType.Operator)
							{
								double n2 = valueStack.Pop();
								double n1 = valueStack.Pop();
								operationStack.Pop();
								double rst = CalculateOperator(ref topEle, n1, n2);
								valueStack.Push(rst);
							}
						}
						else
						{
							i++;
						}

						break;
					default:
						i++;
						break;
				}

			}

			while (operationStack.Count != 0)
			{
				Ele topEle = operationStack.Peek();
				switch (topEle.EleType)
				{
					case EnumEleType.Operator:
						double n2 = valueStack.Pop();
						double n1 = valueStack.Pop();
						operationStack.Pop();
						double rst1 = CalculateOperator(ref topEle, n1, n2);
						valueStack.Push(rst1);
						break;
					case EnumEleType.Function:
						double n = valueStack.Pop();
						operationStack.Pop();
						double rst2 = CalculateFunction(ref topEle, n);
						if (IsCustomFunction(topEle))
							rst2 = CustomDic[topEle.Value].Invoke(n);
						else
							rst2 = CalculateFunction(ref topEle, n);
						valueStack.Push(rst2);
						break;
					default:
						break;
				}
			}

			return Math.Round(valueStack.Peek(), 5);
		}

		public static string Parse(Ele[] eles)
		{
			Stack<Ele> valueStack = new Stack<Ele>();
			Stack<Ele> operationStack = new Stack<Ele>();

			int i = 0;
			int len = eles.Length;
			while (i < len)
			{
				Ele ele = eles[i];
				//入栈操作((-1)^floor(2*(0))*0.5+0.5)
				switch (ele.EleType)
				{
					case EnumEleType.Null:
						i++;
						break;
					case EnumEleType.Constant:
						valueStack.Push(ele);
						//放入时
						if (valueStack.Count > 1)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Operator)
							{
								Ele n2 = valueStack.Pop();
								Ele n1 = valueStack.Pop();
								operationStack.Pop();
								Ele rst = new Ele(n1.Value + topEle.Value + n2.Value, EnumEleType.Number);
								valueStack.Push(rst);
							}
						}
						i++;
						break;
					case EnumEleType.Number:
						valueStack.Push(ele);
						if (valueStack.Count > 1)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Operator)
							{
								Ele n2 = valueStack.Pop();
								Ele n1 = valueStack.Pop();
								operationStack.Pop();
								Ele rst = new Ele(n1.Value + topEle.Value + n2.Value, EnumEleType.Number);
								valueStack.Push(rst);
							}
						}
						i++;
						break;
					case EnumEleType.Operator:
						if (valueStack.Count > 1 && operationStack.Count > 1)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Operator && CompareLowerRank(ele, topEle))
							{
								Ele n2 = valueStack.Pop();
								Ele n1 = valueStack.Pop();
								operationStack.Pop();
								Ele rst = new Ele(n1.Value + topEle.Value + n2.Value, EnumEleType.Number);
								valueStack.Push(rst);
							}
							else
							{
								operationStack.Push(ele);
								i++;
							}
						}
						else
						{
							operationStack.Push(ele);
							i++;
						}
						break;
					case EnumEleType.Function:
						operationStack.Push(ele);
						i++;
						break;
					case EnumEleType.Completed:
						if (valueStack.Count > 0)
						{
							Ele topEle = operationStack.Peek();
							if (topEle.EleType == EnumEleType.Function)
							{
								Ele n = valueStack.Pop();
								operationStack.Pop();
								Ele rst = topEle;
								if (IsCustomFunction(topEle))
									rst = new Ele(CustomDic[rst.Value].Parse(n), EnumEleType.Number);
								else
								{
									if (rst.Value == "empty")
										rst = new Ele($"({n})", EnumEleType.Number);
									else
										rst = new Ele($"{rst.Value}({n})", EnumEleType.Number);
								}

								valueStack.Push(rst);
								i++;
							}
							else if (topEle.EleType == EnumEleType.Operator)
							{
								Ele n2 = valueStack.Pop();
								Ele n1 = valueStack.Pop();
								operationStack.Pop();
								Ele rst = new Ele(n1.Value + topEle.Value + n2.Value, EnumEleType.Number);
								valueStack.Push(rst);
							}
						}
						else
						{
							i++;
						}

						break;
					default:
						i++;
						break;
				}

			}

			while (operationStack.Count != 0)
			{
				Ele topEle = operationStack.Peek();
				switch (topEle.EleType)
				{
					case EnumEleType.Operator:
						Ele n2 = valueStack.Pop();
						Ele n1 = valueStack.Pop();
						operationStack.Pop();
						Ele rst1 = new Ele(n1.Value + topEle.Value + n2.Value, EnumEleType.Number);
						valueStack.Push(rst1);
						break;
					case EnumEleType.Function:
						Ele n = valueStack.Pop();
						operationStack.Pop();
						Ele rst = topEle;
						if (IsCustomFunction(topEle))
							rst = new Ele(CustomDic[rst.Value].Parse(n), EnumEleType.Number);
						else
						{
							if (rst.Value == "empty")
								rst = new Ele($"({n})", EnumEleType.Number);
							else
								rst = new Ele($"{rst.Value}({n})", EnumEleType.Number);
						}

						valueStack.Push(rst);
						i++;
						break;
					default:
						break;
				}
			}

			return valueStack.Peek().Value;
		}


		#endregion
	}

}
