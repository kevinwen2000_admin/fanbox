﻿namespace FanBox.FanBoxLib.Tools
{
	public class Script
	{
		private string language;
		private string code;
		public string Language { get=>this.language; set=>this.language = value; }
		public string Code { get =>this.code; set=>this.code=value; }

		public Script() : this(string.Empty, string.Empty) { }

		public Script(string language, string code)
		{
			this.Language = language;
			this.Code = code;
		}

		public Script(Script script) : this(script.Language, script.Code) { }

		public string GetReturn()
		{
			return "null";
		}
	}
}
