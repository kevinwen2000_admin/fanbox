﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FanBox.FanBoxLib;
namespace FanBox.FanCan
{
	public class Program
	{
		#region main block
		private static void Main(string[] args)
		{
			FanCanCore.IsCommentMode = false;
			//SetShapeProperty("rect", "x", "100");
			try
			{
				if (args.Length == 0)
				{
					FanCanCore.LoadSettings(false);
					FanCanCore.DisplayHelp();
					FanCanCore.RunManual();
				}
				else
				{
					FanCanCore.LoadSettings(true);
					//DisplayHelp();
					FanCanCore.RunAuto(args[0]);
				}
			}
			catch (Exception e)
			{
				if (e.HResult == -2147221021)
				{
					Console.WriteLine("错误：PowerPoint未开启");
					Console.WriteLine("解决方法：打开PowerPoint。");
				}
				else
				{
					Console.WriteLine(e.Message);
				}

			}
			Console.WriteLine("程序结束，请按任意键结束进程");
			Console.ReadKey();
			FanCanCore.CloseSlideShow();
		}
		#endregion
	}
}
